Qualtrics.SurveyEngine.addOnload(function()
{
	document.honorEventLog = [];
	document.honorEventTime = 0;
	document.honorEventCount = 0;
	Qualtrics.SurveyEngine.setJSEmbeddedData('HonorEventLog', "");
	Qualtrics.SurveyEngine.setJSEmbeddedData('HonorTotalTime', "0");
	
	function logHonorEvent(e) {
		console.log("logHonorEvent() got event:");
		console.log(e);
        var currLog = Qualtrics.SurveyEngine.getJSEmbeddedData('HonorEventLog');
        var currTime = Number(Qualtrics.SurveyEngine.getJSEmbeddedData('HonorTotalTime'));
        if (e && ("target" in e) && e.target) {
			const target = e.target;
			console.log("logHonorEvent() got event with target:");
			console.log(target);
            // handle the blur/focus event here
            if (e.type == "blur") {
                // user moved away from this page.
                currLog += ("|blur@" + e.target + "@" + parseFloat(e.timeStamp).toFixed(0));
                Qualtrics.SurveyEngine.setJSEmbeddedData('HonorEventLog', currLog);
                document.honorEventTime = e.timeStamp;
				document.honorEventCount += 1;
            } else if (e.type == "focus") {
                // user returned attention to this page.
                currLog += ("|focus@" + e.target + "@" + parseFloat(e.timeStamp).toFixed(0));
                Qualtrics.SurveyEngine.setJSEmbeddedData('HonorEventLog', currLog);
                currTime += (e.timeStamp - document.honorEventTime);
                Qualtrics.SurveyEngine.setJSEmbeddedData('HonorTotalTime', parseFloat(currTime).toFixed(0));
                document.honorEventTime = e.timeStamp;
			} else {
				console.log("logEssayEvent() received an unknown event type: " + e.type + " with target. This shouldn't happen!");
                console.log(e.target);
			}
		} else {
			// if we are here, it means there's no event target
			if (e && (e.type == "pageReady")) {
				console.log("Received Page Ready event. Right now we are just reporting this at the console, not logging in embedded data.");
			} else {
				console.log("logEssayEvent() received an unknown event type without target: " + e.type + " This shouldn't happen!");
			}
		}
		Qualtrics.SurveyEngine.setJSEmbeddedData('KnowledgeCheckLookAwayCount', document.honorEventCount.toFixed(0));
	}
	document.logHonorEvent = logHonorEvent;

});

Qualtrics.SurveyEngine.addOnReady(function()
{
	document.logHonorEvent(new Event('pageReady'));
    window.addEventListener("blur", document.logHonorEvent);
    window.addEventListener("focus", document.logHonorEvent);

});

Qualtrics.SurveyEngine.addOnUnload(function()
{
	/*Place your JavaScript here to run when the page is unloaded*/

});