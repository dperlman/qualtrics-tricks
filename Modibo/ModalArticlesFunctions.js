	Qualtrics.SurveyEngine.addOnload(function()
	{
		document.supportEventLog = [];
		document.opposeEventLog = [];
		document.supportEventTime = 0;
		document.opposeEventTime = 0;
		document.articleReadCount = {'opp':0, 'sup':0};
		document.doFeeWarn = Qualtrics.SurveyEngine.getJSEmbeddedData('DoFeeWarn');
		console.log("initial setup: doFeeWarn is " + document.doFeeWarn);
		document.doFeeWarn = parseInt(document.doFeeWarn)?true:false;
		console.log("initial setup: doFeeWarn is " + document.doFeeWarn);
		document.readFee = Qualtrics.SurveyEngine.getJSEmbeddedData('ReadFee');

		Qualtrics.SurveyEngine.setJSEmbeddedData('AllEssayEventLog',"");
		Qualtrics.SurveyEngine.setJSEmbeddedData('AllEssayTotalTime',0);
		Qualtrics.SurveyEngine.setJSEmbeddedData('OpposeEssayEventLog', "");
		Qualtrics.SurveyEngine.setJSEmbeddedData('OpposeEssayTotalTime', 0);
		Qualtrics.SurveyEngine.setJSEmbeddedData('SupportEssayEventLog', "");
		Qualtrics.SurveyEngine.setJSEmbeddedData('SupportEssayTotalTime', 0);
		
		// get these texts, but set defaults here for testing purposes
		document.readFeePrewarn = Qualtrics.SurveyEngine.getJSEmbeddedData('ReadFeePrewarn');
		if (document.readFeePrewarn && document.readFeePrewarn.replace(/ /g, '')) {
			// that means it's not empty and it's not just blank space
			// so we are good here
		} else {
			// it is empty, or blank
			document.readFeePrewarn = "You are about to read one article for free. If you want to read the second article after this, you will be charged a fee of " + document.readFee;
		}

		document.readFeeWarn = Qualtrics.SurveyEngine.getJSEmbeddedData('ReadFeeWarn');
		if (document.readFeeWarn && document.readFeeWarn.replace(/ /g, '')) {
			// that means it's not empty and it's not just blank space
			// so we are good here
		} else {
			// it is empty, or blank
			document.readFeeWarn = "Since you already read the other article for free, you will now be charged a fee of " + document.readFee + " to read this article. You can read it as many times as you want once you pay the fee.";
		}

		
		// this is based on the destructuring assignment trick I learned here:
		// https://stackoverflow.com/questions/34698905/how-can-i-clone-a-javascript-object-except-for-one-key
		// thanks to Ilya Palkin. This is honestly pretty amazing
		// ok well it didn't work, toooooo new for Qualtrics lolololol
		function otherOne(anObj, wrongOne) {
			var keys = Object.keys(anObj);
			var newKeys = keys.filter(x => x != wrongOne);
			if (newKeys.length == keys.length) {
				console.log("otherOne(): given item was not found in object!");
			} else if (newKeys.length == 0) {
				console.log("otherOne(): for some reason nothing was left after removing given item!");
			} else if (newKeys.length > 1) {
				console.log("otherOne(): output has more than one item. This is fine if that's what you want but be careful!");
			}
			let otherKey = newKeys[0];
			let other = anObj[otherKey];
			//let {[wrongOne]: _, ...others} = anObj;
			//let otherKey = Object.keys(others)[0];
			//let other = others[otherKey];
			return {'key':otherKey, 'val':other};
		}


		function logEssayEvent(e) {
			console.log("logEssayEvent() got event:");
			console.log(e);
			var currLog = Qualtrics.SurveyEngine.getJSEmbeddedData('AllEssayEventLog');
			var currTime = Qualtrics.SurveyEngine.getJSEmbeddedData('AllEssayTotalTime');
			var currOpposeLog = Qualtrics.SurveyEngine.getJSEmbeddedData('OpposeEssayEventLog');
			var currOpposeTime = Number(Qualtrics.SurveyEngine.getJSEmbeddedData('OpposeEssayTotalTime'));
			var currSupportLog = Qualtrics.SurveyEngine.getJSEmbeddedData('SupportEssayEventLog');
			var currSupportTime = Number(Qualtrics.SurveyEngine.getJSEmbeddedData('SupportEssayTotalTime'));
			if ("target" in e && e.target) {
				const target = e.target;
				console.log("logEssayEvent() got event with target:");
				console.log(target);
				if (target.classList.contains("opposeOpen")) {
					console.log(currOpposeTime);
					currLog += ("|opposeOpen@" + e.type + "@" + e.timeStamp.toFixed(0));
					currOpposeLog += ("|opposeOpen@" + e.type + "@" + e.timeStamp.toFixed(0));
					document.opposeEventTime = e.timeStamp;
				} else if (target.classList.contains("supportOpen")) {
					console.log(currSupportTime);
					currLog += ("|supportOpen@" + e.type + "@" + e.timeStamp.toFixed(0));
					currSupportLog += ("|supportOpen@" + e.type + "@" + e.timeStamp.toFixed(0));
					document.supportEventTime = e.timeStamp;
				} else if (target.classList.contains("opposeClose")) {
					console.log(currOpposeTime);
					currLog += ("|opposeClose@" + e.type + "@" + e.timeStamp.toFixed(0));
					currOpposeLog += ("|opposeClose@" + e.type + "@" + e.timeStamp.toFixed(0));
					currOpposeTime += (e.timeStamp - document.opposeEventTime);
					currTime += (e.timeStamp - document.opposeEventTime);
					console.log("currOpposeTime about to be saved: " + currOpposeTime.toFixed(0));
					document.opposeEventTime = e.timeStamp;
				} else if (target.classList.contains("supportClose")) {
					console.log(currSupportTime);
					currLog += ("|supportClose@" + e.type + "@" + e.timeStamp.toFixed(0));
					currSupportLog += ("|supportClose@" + e.type + "@" + e.timeStamp.toFixed(0));
					currSupportTime += (e.timeStamp - document.supportEventTime);
					currTime += (e.timeStamp - document.supportEventTime);
					console.log("currSupportTime about to be saved: " + currSupportTime.toFixed(0));
					document.supportEventTime = e.timeStamp;
				} else {
					console.log("logEssayEvent() received an unknown event type with target: " + e.type + " This shouldn't happen!");
				}
			} else {
				// if we are here, it means there's no event target
				if (String(e.type).toLowerCase() == "pageready") {
					console.log("Received Page Ready event. Right now we are just reporting this at the console, not logging in embedded data.");
				} else {
					console.log("logEssayEvent() received an unknown event type without target: " + e.type + " This shouldn't happen!");
				}
			}
			Qualtrics.SurveyEngine.setJSEmbeddedData('AllEssayEventLog', currLog);
			Qualtrics.SurveyEngine.setJSEmbeddedData('AllEssayTotalTime', parseFloat(currTime).toFixed(0));
			Qualtrics.SurveyEngine.setJSEmbeddedData('OpposeEssayEventLog', currOpposeLog);
			Qualtrics.SurveyEngine.setJSEmbeddedData('OpposeEssayTotalTime', parseFloat(currOpposeTime).toFixed(0));
			Qualtrics.SurveyEngine.setJSEmbeddedData('SupportEssayEventLog', currSupportLog);
			Qualtrics.SurveyEngine.setJSEmbeddedData('SupportEssayTotalTime', parseFloat(currSupportTime).toFixed(0));
		}
		// set this function to a document property so we can access it globally
		document.logEssayEvent = logEssayEvent;

		// Functions for handling the modal popups for article and warnings

		function showArticle(articleType) {
			// this actually shows the requested article. Also updates the record of how many times it was shown.
			console.log("showArticle() got articleType " + articleType);
			var divId;
			if (String(articleType).toLowerCase() == 'sup') {
				divId = "#myModalSupport";
			} else if (String(articleType).toLowerCase() == 'opp') {
				divId = "#myModalOppose";
			}
			const article = new bootstrap.Modal(document.querySelector(divId), {});
			article.show();
			document.articleReadCount[articleType] += 1;
			return article;
		}

		function showWarning(articleType) {
			// this shows the warning dialog
			console.log("showWarning() got articleType |" + articleType + "|");
			var divId;
			if (String(articleType).toLowerCase() == 'sup') {
				divId = "#warnModalCenterSup";
			} else if (String(articleType).toLowerCase() == 'opp') {
				divId = "#warnModalCenterOpp";
			} else {
				console.log("showWarning(): for some reason the article type isn't working: " + articleType);
			}
			const warning = new bootstrap.Modal(document.querySelector(divId), {});
			warning.show();
			return warning;
		}
		
		// launch article function that smartly determines whether to show the warning dialog
		function launchArticle(articleType) {
			// this is the function that is called by the user clicking the "read article" link.
			// it intelligently determines what behavior is appropriate for this version and the current state.
			console.log("launchArticle() got article type " + articleType);
			const thisReadCount = document.articleReadCount[articleType];
			const otherInfo = otherOne(document.articleReadCount, articleType);
			const otherArtType = otherInfo.key;
			const otherReadCount = otherInfo.val;
			console.log("launchArticle(): Article types:read counts: this: " + articleType + ":" + thisReadCount + ", other: " + otherArtType + ":" + otherReadCount);
			//const totalReadCount = document.supportReadCount + document.opposeReadCount;
			if (!document.doFeeWarn || (document.articleReadCount[articleType] > 0)) {
				// if we are not doing the warning at all, or else if the requested one has already been viewed, then no more warnings are necessary
				showArticle(articleType);
				// anything past this block will necessarily have document.doFeeWarn is true
			} else if (document.articleReadCount[articleType] == 0) {
				// the requested one has not been read yet, so launch the baseline warning modal
				// if it was necessary, the message text was already updated by the code in the button handler at the end of the dialog
				showWarning(articleType);
			} else {
				// that should have covered all the possibilities, so if we get here it's an error and I'll log it
				console.log("launchArticle(): got to end of if-else-else block. this should not happen. articleType: " + articleType + " articleReadCount: " + articleReadCount);
			}
		}
		document.launchArticle = launchArticle;

		function warnDialogHandler(articleType) {
			// this is the function that runs from the warning dialog when you click the yes, read button.
			// here's how it works:
			// the button's data-dismiss property means that it dismisses itself immediately from the screen
			// then the setTimeout in the onclick handler launches the new modal after 200ms
			// which is long enough to let bootstrap.Modal library do whatever it needs to do behind the scenes regarding window scrolling
			// then also this runs at that time.
			// that means that we can change the innerText of the warning dialog here without worrying about it flashing on the screen
			// because it's already dismissed 200ms before this runs
			console.log("warnDialogHandler() got article type " + articleType);
			if ((document.articleReadCount.opp == 0) && (document.articleReadCount.sup == 0)) {
				// nothing has been read yet, this is the first time saying yes.
				// this is one category of condition but we still need to know which one is which
				if (articleType == 'opp') {
					document.getElementById('readFeeWarnSup').innerText = document.readFeeWarn;
				} else if (articleType == 'sup') {
					document.getElementById('readFeeWarnOpp').innerText = document.readFeeWarn;
				} else {
					console.log('warnDialogHandler() ERROR got an unknown article type, this should never happen! ' + articleType);
				}
			} else {
				// we disable this elsewhere when the warnings are done,
				// so the only way we ever end up in this else block is if we are showing the full warning
				// which means it's time to actually charge the fee
				console.log("warnDialogHandler() is now setting the embedded data variable PaymentSubtractValue to " + document.readFee);
				Qualtrics.SurveyEngine.setJSEmbeddedData('PaymentSubtractValue', parseFloat(document.readFee.match(/\d+/)[0]).toFixed(2));
			}
			document.articleReadCount[articleType] += 1;
			Qualtrics.SurveyEngine.setJSEmbeddedData('OpposeEssayReadCount', document.articleReadCount['opp'].toFixed(0));
			Qualtrics.SurveyEngine.setJSEmbeddedData('SupportEssayReadCount', document.articleReadCount['sup'].toFixed(0));
			showArticle(articleType);
		}
		document.warnDialogHandler = warnDialogHandler;

	});

	Qualtrics.SurveyEngine.addOnReady(function()
	{
		document.logEssayEvent(new Event('pageReady'));
		const essayButtons = document.querySelectorAll(".supportOpen,.supportClose,.opposeOpen,.opposeClose");
		console.log("essayButtons:");
		console.log(essayButtons);
		for (b of essayButtons) {
			console.log("adding pointerDown event");
			b.addEventListener("pointerDown", document.logEssayEvent);
			console.log("adding click event")
			b.addEventListener("click", document.logEssayEvent);
			console.log(b);
		}
		// set the dialog text to the right text
		if (document.doFeeWarn) {
			document.getElementById('readFeeWarnOpp').innerText = document.readFeePrewarn;
			document.getElementById('readFeeWarnSup').innerText = document.readFeePrewarn;
		}
	});

	Qualtrics.SurveyEngine.addOnUnload(function()
	{
		/*Place your JavaScript here to run when the page is unloaded*/

	});