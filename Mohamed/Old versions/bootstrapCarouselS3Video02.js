// The challenge that this file is meant to solve was showing phone-format (portrait) videos 
// in a Bootstrap carousel slider, with good aspect ratio formatting,
// smart autoplay (that takes the carousel into account),
// and recording play time of each video.
// For this version the videos are hosted in an S3 bucket with Cloudfront cacheing.
// They must already be in portrait aspect ratio, I am not including code here to crop them, although that is also possible.
// The particular challenge of this new version is how to get it to semi-autoplay with sound on mobile.
// I had it working perfectly on desktop but mobile restricts autoplay to silent.
// What we are trying to do here is to click once to start, then it's autoplay from there.



Qualtrics.SurveyEngine.addOnload(function()
{
	document.honorEventLog = [];
	document.honorEventTime = 0;
	Qualtrics.SurveyEngine.setJSEmbeddedData('HonorEventLog', "");
	Qualtrics.SurveyEngine.setJSEmbeddedData('HonorTotalTime', 0);
    // now find out if we want to reset them when moved away and then back
    const resetOnReturn = Qualtrics.SurveyEngine.getEmbeddedData('resetOnReturn');
    document.resetOnReturn = (String(resetOnReturn).toLowerCase() == 'false')?false:true; // default is true if it's anything other than explicitly "false" in the embedded data

	function logHonorEvent(e) {
		console.log("logHonorEvent() got event:");
		console.log(e);
        var currLog = Qualtrics.SurveyEngine.getEmbeddedData('HonorEventLog');
        var currTime = Number(Qualtrics.SurveyEngine.getEmbeddedData('HonorTotalTime'));
        if (e && ("target" in e) && e.target) {
			const target = e.target;
			console.log("logHonorEvent() got event with target:");
			console.log(target);
            // handle the blur/focus event here
            if (e && (e.type == "blur")) {
                // they moved away from this page.
                currLog += ("|blur@" + e.target + "@" + e.timeStamp);
                Qualtrics.SurveyEngine.setEmbeddedData('HonorEventLog', currLog);
                document.honorEventTime = e.timeStamp;
            } else if (e.type == "focus") {
                // they returned attention to this page.
                currLog += ("|focus@" + e.target + "@" + e.timeStamp);
                Qualtrics.SurveyEngine.setJSEmbeddedData('HonorEventLog', currLog);
                currTime += (e.timeStamp - document.honorEventTime);
                Qualtrics.SurveyEngine.setEmbeddedData('HonorTotalTime', currTime);
                document.honorEventTime = e.timeStamp;
			} else {
				console.log("logEssayEvent() received an unknown event type: " + e.type + " with target. This shouldn't happen!");
                console.log(e.target);
			}
		} else {
			// if we are here, it means there's no event target
			if (e && (e.type == "pageReady")) {
				console.log("Received Page Ready event. Right now we are just reporting this at the console, not logging in embedded data.");
			} else {
				console.log("logEssayEvent() received an unknown event type without target: " + e.type + " This shouldn't happen!");
			}
		}
	}

	document.logHonorEvent = logHonorEvent;
});


Qualtrics.SurveyEngine.addOnReady(function()
{
    // this shoudl make swipe work on mobile??
    document.querySelector('.carousel').bcSwipe({ threshold: 50 });


    function vidNumber(e, prefix) {
        let target;
        if (!prefix) {
            prefix = "c1v"; // hard coded default, same as set above (yeah this is bad coding practice)
        }
        var vidClass;
        if ('vidNum' in e) {
            // if this is a fake event that we made for internal sync purposes, just use the number directly
            console.log('vidNumber() received a dummy event that had its own vidNum property, using that : ' + e.vidNum);
            return {'class':null, 'vidNum':e.vidNum}
        }
        if ('element' in e) {
            target = e.element;
        } else if ('target' in e) {
            target = e.target;
        } else if ('srcElement' in e) {
            target = e.srcElement;
        } else if ('classList' in e) {
            target = e;
        } else {
            console.log('vidNumber(): passed element/event does not have element or target or srcElement or classList properties, fail!!!');
            console.log(e);
            return {'class':null, 'vidNum':null}
        }
        // note that I've hard coded the prefix you have to use here, this could be a problem eventually
        if (('id' in target) && target.id.startsWith(prefix)) {
            vidClass = String(target.id);
        } else {
            for (c of target.classList) {
                if (String(c).startsWith(prefix)) {
                    vidClass = String(c);
                    break;
                }
            }
        }
        vidSuffix = vidClass.slice(prefix.length);
        const fullName = getFullName(target);
        //console.log('vidNumber is looking at element with full name: ' + fullName);
        // great, now get the number
        var vidNum = parseInt(vidSuffix.match(/\d+/)[0]);
        console.log('vidNumber() found vidNum: ' + vidNum + ' for element: ' + fullName);
        return {'class':fullName, 'vidNum':vidNum};
    }

    function setEmbeddedData(prefix, number, value) {
        const name = prefix + parseInt(number).toString();
        Qualtrics.SurveyEngine.setEmbeddedData(name, value);
        return name;
    }

    function getFullName(element) {
        const tag = element.localName;
        const id = element.identify();
        const classes = [...element.classList].join('.');
        const fullName = tag + '#' + id + '.' + classes;
        console.log('getFullName() got a full name: ' + fullName);
        return fullName;
    }

    function autoPlayObserverFunction(entries) {
        for (entry of entries) {
            //console.log('autoPlayObserverFunction() current entry being checked:');
            //console.log(entry);
            let vNum = vidNumber(entry).vidNum;
            //let timeStamp = window.performance.now();
            if (entry.isIntersecting) {
                if (document.resetOnReturn) {
                    document.videos[vNum-1].currentTime = 0;
                }
                console.log("autoPlayObserverFunction() about to start play for video number " + vNum);
                document.videos[vNum-1].play();
            } else {
                console.log("autoPlayObserverFunction() about to pause for video number " + vNum);
                document.videos[vNum-1].pause();
            }
        }
    }

    function playCountEventFunction(e) {
        var vidClass, vidNum;
        vidClass = vidNumber(this);
        vidNum = vidClass.vidNum;
        vidClass = vidClass.class;
        //console.log(vidNum);
        //console.log(vidClass);
        document.playing[vidNum-1] = true;
        document.playCount[vidNum-1]++;
        document.prevPlayTime[vidNum-1] = window.performance.now();
        console.log("play event for video " + vidClass + " at timestamp " + document.prevPlayTime[vidNum-1] + " for current play count of " + document.playCount[vidNum-1]);
        setEmbeddedData('playCount', vidNum, document.playCount[vidNum-1]);
    }

    function pauseCountEventFunction(e) {
        console.log("Pause event handler triggered!");
        var vidClass, vidNum;
        vidClass = vidNumber(this);
        vidNum = vidClass.vidNum;
        vidClass = vidClass.class;
        //console.log(vidNum);
        //console.log(vidClass)
        console.log("Pause event is for video " + vidClass + " at timestamp " + window.performance.now());
        var currPlayTime = window.performance.now() - document.prevPlayTime[vidNum-1];
        document.playing[vidNum-1] = false;
        document.playTime[vidNum-1] += currPlayTime;
        console.log("this play time for video " + vidNum + " was " + currPlayTime + " for a total of " + document.playTime[vidNum-1]);
        setEmbeddedData('playTime', vidNum, Math.round(document.playTime[vidNum-1]));
        //console.log(Qualtrics.SurveyEngine.getEmbeddedData('playTime' + vidNum));
    }

    function userInitiatedSetup(e) {
        // this goes in an event listener for the document.body
        const n = document.videos.length;
        //console.log("userInitiatedSetup(): sent play signal to " + n + " video players.");
    }

    function finalCleanup(e) {
        // this goes in an event listener for the next button
        console.log("Next button was clicked; recording time for last video.");
        for (i=0; i<document.videos.length; i++) {
            if (document.playing[i]) {
                console.log('Video number ' + (i+1) + ' was playing, recording it as ended');
                var currPlayTime = window.performance.now() - document.prevPlayTime[i];
                document.playTime[i] += currPlayTime;
                setEmbeddedData('playTime', i+1, Math.round(document.playTime[i]));
                document.playing[i] = false;
                document.players[i].pause();
            }
            console.log("playTime" + (i+1) + ": " + Qualtrics.SurveyEngine.getEmbeddedData('playTime' + (i+1))); // this still might not work due to delay in pause handler.
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    // Finished with function definitions, now act.

    // first set a handler on the next button to save the last video.
    // have to do this because it never ended up working from the addOnPageSubmit section.
    // my testing indicated that the 5 ms delay in the player was long enough that setEmbeddedData didn't work anymore.
    document.getElementById('NextButton').addEventListener("click", finalCleanup);

    // now set handler on the whole body so that as soon as someone starts the first video playing,
    // it turns on autoplay for all the videos. hopefully this works? if this doesn't work there are other things I can try.
    document.body.addEventListener('click', userInitiatedSetup);
    
    // now get all the video players and their associated iframes and set up global storage for our purposes
    document.videos = [...document.querySelectorAll('video.video-js.vjs-default-skin')];
    console.log(document.videos);
    const numVids = document.videos.length;
    // document.players = new Array(numVids);
    // for (let i=0; i<numVids; i++) {
    //     let frame = document.videos[i];
    //     let numberedId = 'c1v' + (i+1).toString();
    //     let player = document.querySelector('video#' + numberedId);
    //     console.log(player);
    //     document.players[i] = player;
    //     setEmbeddedData('playCount', i+1, 0);
    //     setEmbeddedData('playTime', i+1, 0);
    // }

    document.playTime = new Array(numVids).fill(0);
    document.prevPlayTime = new Array(numVids).fill(0);
    document.playCount = new Array(numVids).fill(0);
    document.playing = new Array(numVids).fill(false);
    document.interactionChecks = [];

    // Now set an observer to let us start and stop videos when the carousel brings them into view
    // The purpose of the observer is to watch each individual player iframe 
    // and see when it is visible. It will stop playing when it's not visible and start playing when it is visible.
    const observer = new IntersectionObserver(autoPlayObserverFunction);
    // IntersectionObserver API works really well. Here's documentation
    // https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API

    var player, video;
    //console.log('document.players:');
    //console.log(document.players);
    for (let i=0; i<numVids; i++) {
        //player = document.players[i];
        video = document.videos[i];
        observer.observe(video);
        //frame.parentElement.parentElement.addEventListener('pointerDown', interactionChecker);
        video.addEventListener('play', playCountEventFunction);
        video.addEventListener('pause', pauseCountEventFunction);
        video.addEventListener('ended', pauseCountEventFunction);
    }

    // now try one trick to try to get it to autoplay
    
    function testEventFunction(e) {
        console.log('testEventFunction() triggered. logging event and this-keyword:');
        console.log(e);
        console.log(this);
        //window.alert('testEventFunction: ');
        // now try loading a video in the second player
        //document.players[1].loadVideo(965299723);

    }
    //player = document.players[0] // the first one only
    //player.on('play', testEventFunction);

    document.logHonorEvent(new Event('pageReady'));
    window.addEventListener("blur", document.logHonorEvent);
    window.addEventListener("focus", document.logHonorEvent);

});


Qualtrics.SurveyEngine.addOnPageSubmit(function()
{
	// as far as I can tell, setEmbeddedData only works in the first few milliseconds.
    // 20 ms seems to be too long; the embedded data have already been saved at that point and no further saves are successful.
    // even 5ms seems to be too long.
    // The solution, I guess, is to put the save into the next button...
    // annoying that this is asynchronous like that
    console.log("Page submitting; hopefully it was already all recorded by the next button handler!");
});
