// The challenge that this file is meant to solve was showing phone-format (portrait) videos 
// in a Qualtrics matrix carousel, with good aspect ratio formatting,
// smart autoplay (that takes the carousel into account),
// and recording play time of each video.
// For this version the videos are on Vimeo.
// They must already be in portrait aspect ratio, I am not including code here to crop them, although that is also possible.
// The css here should go into the custom style section of the survey formatting.
// Note that this will affect the whole survey, not just the one question with the matrix carousel.

/*
.Skin .QuestionOuter.Matrix {
    max-width: 100% !important;
}

#SkinContent {
mmin-width:1200px;
}

.CarouselCardFrame {
height:480px !important;
width: 270px !important;
margin:0px !important;
}

.CarouselCardContainer {
height:480px;
width: 270px;
padding:0px !important;
}

.SkinInner {
padding-top:0px !important;
}

div#Logo {
display:none !important;
}

label.QuestionText {
display:none !important;
}

// Note that this next part was for YouTube, I'm leaving it here for now to store it, but DO NOT include this part for Vimeo
.video-background {
  position: absolute;
  top: -230px;
  overflow: hidden;
  width: 270px;
  height: 480px;

  iframe {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 853px;
    height: 480px;
    transform: translate(-50%, -50%);
  }
}
*/

// Also, here's how to embed the videos. Use the vimeo share button to get the embed code.
// Paste it into the HTML for the item text. You might need rich text editor first and then get the HTML button from there.
// Then edit the embed code to match this format. 
// Note that giving a class to the iframe is important so we can refer to it in the CSS.
/*
<iframe class="portraitVideo" src="https://player.vimeo.com/video/965303919?h=28889a069f&autoplay=1&loop=1&autopause=0" width="270" height="480" frameborder="0" allow="autoplay" autoplay></iframe>
*/

// Finally, paste the following into the html view of the top text for the Qualtrics question.
// This loads the Vimeo player API which is necessary for using Vimeo videos embedded.
/*
<script src="https://player.vimeo.com/api/player.js"></script>
*/

// NOTE: I'm finding that the Vimeo player video events don't follow the Event interface.
// There doesn't seem to be any way to tell which one started playing/pause so I can't track the time.
// I figured out a workaroung though! While inside the event handler, the "this" keyword captures the
// element that sent the event.

Qualtrics.SurveyEngine.addOnload(function()
{
	document.honorEventLog = [];
	document.honorEventTime = 0;
	Qualtrics.SurveyEngine.setJSEmbeddedData('HonorEventLog', "");
	Qualtrics.SurveyEngine.setJSEmbeddedData('HonorTotalTime', 0);
    // now find out if we want to reset them when moved away and then back
    const resetOnReturn = Qualtrics.SurveyEngine.getEmbeddedData('resetOnReturn');
    document.resetOnReturn = (String(resetOnReturn).toLowerCase() == 'true');

	function logHonorEvent(e) {
		console.log("logHonorEvent() got event:");
		console.log(e);
        var currLog = Qualtrics.SurveyEngine.getJSEmbeddedData('HonorEventLog');
        var currTime = Number(Qualtrics.SurveyEngine.getJSEmbeddedData('HonorTotalTime'));
        if (e && ("target" in e) && e.target) {
			const target = e.target;
			console.log("logHonorEvent() got event with target:");
			console.log(target);
            // handle the blur/focus event here
            if (e && (e.type == "blur")) {
                // they moved away from this page.
                currLog += ("|blur@" + e.target + "@" + e.timeStamp);
                Qualtrics.SurveyEngine.setJSEmbeddedData('HonorEventLog', currLog);
                document.honorEventTime = e.timeStamp;
            } else if (e.type == "focus") {
                // they returned attention to this page.
                currLog += ("|focus@" + e.target + "@" + e.timeStamp);
                Qualtrics.SurveyEngine.setJSEmbeddedData('HonorEventLog', currLog);
                currTime += (e.timeStamp - document.honorEventTime);
                Qualtrics.SurveyEngine.setJSEmbeddedData('HonorTotalTime', currTime);
                document.honorEventTime = e.timeStamp;
			} else {
				console.log("logEssayEvent() received an unknown event type: " + e.type + " with target. This shouldn't happen!");
                console.log(e.target);
			}
		} else {
			// if we are here, it means there's no event target
			if (e && (e.type == "pageReady")) {
				console.log("Received Page Ready event. Right now we are just reporting this at the console, not logging in embedded data.");
			} else {
				console.log("logEssayEvent() received an unknown event type without target: " + e.type + " This shouldn't happen!");
			}
		}
	}

	document.logHonorEvent = logHonorEvent;
});


Qualtrics.SurveyEngine.addOnReady(function()
{
    // first add a handler to the document.getElementById('NextButton').addEventListener("click", setValue);


    function vidNumber(e, prefix) {
        // The Vimeo documentation says that player events are "identical to" HTML5 video events but this is not true.
        // They have the same NAMES but the events have all different properties.
        //console.trace(); // trying to figure out why this gets called so much
        let target;
        if (!prefix) {
            prefix = "carouselVideoNum"; // hard coded default, same as set above (yeah this is bad coding practice)
        }
        var vidClass;
        if ('vidNum' in e) {
            // if this is a fake event that we made for internal sync purposes, just use the number directly
            console.log('vidNumber() received a dummy event that had its own vidNum property, using that : ' + e.vidNum);
            return {'class':null, 'vidNum':e.vidNum}
        }
        if ('element' in e) {
            target = e.element;
        } else if ('target' in e) {
            target = e.target;
        } else if ('srcElement' in e) {
            target = e.srcElement;
        } else {
            console.log('vidNumber(): passed element/event does not have element or target or srcElement properties, fail!!!');
            console.log(e);
            return {'class':null, 'vidNum':null}
        }
        for (c of target.classList) {
            if (String(c).startsWith(prefix)) {
                // note that I've hard coded the prefix you have to use here, this could be a problem eventually
                vidClass = String(c);
                break;
            }
        }
        const fullName = getFullName(target);
        //console.log('vidNumber is looking at element with full name: ' + fullName);
        // great, now get the number
        var vidNum = parseInt(vidClass.match(/\d+/)[0]);
        console.log('vidNumber() found vidNum: ' + vidNum + ' for element: ' + fullName);
        return {'class':fullName, 'vidNum':vidNum};
    }

    function setEmbeddedData(prefix, number, value) {
        const name = prefix + parseInt(number).toString();
        Qualtrics.SurveyEngine.setEmbeddedData(name, value);
        return name;
    }

    function getFullName(element) {
        const tag = element.localName;
        const id = element.identify();
        const classes = [...element.classList].join('.');
        const fullName = tag + '#' + id + '.' + classes;
        return fullName;
    }

    function autoPlayObserverFunction(entries) {
        for (entry of entries) {
            //console.log('autoPlayObserverFunction() current entry being checked:');
            //console.log(entry);
            let vNum = vidNumber(entry).vidNum;
            //let timeStamp = window.performance.now();
            if (entry.isIntersecting) {
                if (document.resetOnReturn) {
                    document.players[vNum-1].setCurrentTime(0);
                }
                document.players[vNum-1].play();
            } else {
                document.players[vNum-1].pause();
            }
        }
    }

    function playCountEventFunction(e) {
        var vidClass, vidNum;
        vidClass = vidNumber(this);
        vidNum = vidClass.vidNum;
        vidClass = vidClass.class;
        //console.log(vidNum);
        //console.log(vidClass);
        document.playing[vidNum-1] = true;
        document.playCount[vidNum-1]++;
        document.prevPlayTime[vidNum-1] = window.performance.now();
        console.log("play event for video " + vidClass + " at timestamp " + document.prevPlayTime[vidNum-1] + " for current play count of " + document.playCount[vidNum-1]);
        setEmbeddedData('playCount', vidNum, document.playCount[vidNum-1]);
    }

    function pauseCountEventFunction(e) {
        console.log("Pause event handler triggered!");
        var vidClass, vidNum;
        vidClass = vidNumber(this);
        vidNum = vidClass.vidNum;
        vidClass = vidClass.class;
        //console.log(vidNum);
        //console.log(vidClass)
        console.log("Pause event is for video " + vidClass + " at timestamp " + window.performance.now());
        var currPlayTime = window.performance.now() - document.prevPlayTime[vidNum-1];
        document.playing[vidNum-1] = false;
        document.playTime[vidNum-1] += currPlayTime;
        console.log("this play time for video " + vidNum + " was " + currPlayTime + " for a total of " + document.playTime[vidNum-1]);
        setEmbeddedData('playTime', vidNum, Math.round(document.playTime[vidNum-1]));
        //console.log(Qualtrics.SurveyEngine.getEmbeddedData('playTime' + vidNum));
    }

    function userInitiatedSetup(e) {
        // this goes in an event listener for the document.body

    }

    function finalCleanup(e) {
        // this goes in an event listener for the next button
        console.log("Next button was clicked; recording time for last video.");
        for (i=0; i<document.players.length; i++) {
            if (document.playing[i]) {
                console.log('Video number ' + (i+1) + ' was playing, recording it as ended');
                var currPlayTime = window.performance.now() - document.prevPlayTime[i];
                document.playTime[i] += currPlayTime;
                setEmbeddedData('playTime', i+1, Math.round(document.playTime[i]));
                document.playing[i] = false;
                document.players[i].pause();
            }
            console.log("playTime" + (i+1) + ": " + Qualtrics.SurveyEngine.getEmbeddedData('playTime' + (i+1))); // this still might not work due to delay in pause handler.
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    // Finished with function definitions, now act.

    // first set a handler on the next button to save the last video.
    // have to do this because it never ended up working from the addOnPageSubmit section.
    // my testing indicated that the 5 ms delay in the player was long enough that setEmbeddedData didn't work anymore.
    document.getElementById('NextButton').addEventListener("click", finalCleanup);

    // now set handler on the whole body so that as soon as someone starts the first video playing,
    // it turns on autoplay for all the videos. hopefully this works? if this doesn't work there are other things I can try.
    document.body.addEventListener('click', userInitiatedSetup);
    
    // now get all the video players and their associated iframes and set up global storage for our purposes
    document.videoFrames = [...document.querySelectorAll('iframe.portraitVideo')];
    //console.log(document.videoFrames);
    const numVids = document.videoFrames.length;
    document.players = new Array(numVids);
    for (let i=0; i<numVids; i++) {
        let frame = document.videoFrames[i];
        let numberedClass = 'carouselVideoNum' + (i+1).toString();
        frame.classList.add(numberedClass);
        frame.id = numberedClass;
        //console.log(frame);
        let player = new Vimeo.Player(frame);
        //console.log(player);
        document.players[i] = player;
        setEmbeddedData('playCount', i+1, 0);
        setEmbeddedData('playTime', i+1, 0);
    }
    // per the documentation for the Vimeo player library
    // https://developer.vimeo.com/player/sdk/basics

    document.playTime = new Array(numVids).fill(0);
    document.prevPlayTime = new Array(numVids).fill(0);
    document.playCount = new Array(numVids).fill(0);
    document.playing = new Array(numVids).fill(false);
    document.interactionChecks = [];

    // Now set an observer to let us start and stop videos when the carousel brings them into view
    // The purpose of the observer is to watch each individual player iframe 
    // and see when it is visible. It will stop playing when it's not visible and start playing when it is visible.
    const observer = new IntersectionObserver(autoPlayObserverFunction);
    // IntersectionObserver API works really well. Here's documentation
    // https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API

    var player, frame;
    //console.log('document.players:');
    //console.log(document.players);
    for (let i=0; i<numVids; i++) {
        player = document.players[i];
        frame = document.videoFrames[i];
        observer.observe(frame);
        //frame.parentElement.parentElement.addEventListener('pointerDown', interactionChecker);
        player.on('play', playCountEventFunction);
        player.on('pause', pauseCountEventFunction);
        player.on('ended', pauseCountEventFunction);
        //player.on('emptied', pauseCountEventFunction);
    }
    // now try one trick to try to get it to autoplay
    
    function testEventFunction(e) {
        console.log('testEventFunction() triggered. logging event and this-keyword:');
        console.log(e);
        console.log(this);
        //window.alert('testEventFunction: ');
        // now try loading a video in the second player
        document.players[1].loadVideo(965299723);

    }
    player = document.players[0] // the first one only
    player.on('play', testEventFunction);

    document.logHonorEvent(new Event('pageReady'));
    window.addEventListener("blur", document.logHonorEvent);
    window.addEventListener("focus", document.logHonorEvent);

});


Qualtrics.SurveyEngine.addOnPageSubmit(function()
{
	// as far as I can tell, setEmbeddedData only works in the first few milliseconds.
    // 20 ms seems to be too long; the embedded data have already been saved at that point and no further saves are successful.
    // even 5ms seems to be too long.
    // The solution, I guess, is to put the save into the next button...
    // annoying that this is asynchronous like that
    console.log("Page submitting; hopefully it was already all recorded by the next button handler!");
});
