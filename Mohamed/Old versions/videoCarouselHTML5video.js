// The challenge that this file is meant to solve was showing phone-format (portrait) videos 
// in a Qualtrics matrix carousel, with good aspect ratio formatting,
// smart autoplay (that takes the carousel into account),
// and recording play time of each video.
// The css here should go into the custom style section of the survey formatting.
// Note that this will affect the whole survey, not just the one question with the matrix carousel.

/*
.Skin .QuestionOuter.Matrix {
    max-width: 100% !important;
}

#SkinContent {
min-width:1200px;
}

.CarouselCardFrame {
height:1000px !important;
}

.CarouselCardContainer {
padding:0px !important;
}

.SkinInner {
padding-top:0px !important;
}

div#Logo {
display:none !important;
}

label.QuestionText {
display:none !important;
}
*/

// Also, here's how to embed the videos. Use the Rich Text Editor to embed it from the media library.
// Then edit the embed code to match this format:
/*
<video class="qmedia carouselvid1" controls="true" loop="true" height="890" preload="auto" width="500"><source src="https://stanfordgsb.qualtrics.com/CP/File.php?F=F_u8uBJGNecgfwopZ" type="video/mp4" /><embed align="middle" bgcolor="white" class="qmedia" controller="true" height="890" pluginspage="http://www.apple.com/quicktime/download/" src="https://stanfordgsb.qualtrics.com/CP/File.php?F=F_u8uBJGNecgfwopZ" type="video/quicktime" width="500" /></video>
*/


Qualtrics.SurveyEngine.addOnload(function()
{
	/*Place your JavaScript here to run when the page loads*/

});

Qualtrics.SurveyEngine.addOnReady(function()
{
    const videos = document.querySelectorAll('video');
    document.playTime = new Array(videos.length).fill(0);
    document.prevPlayTime = new Array(videos.length).fill(0);
    document.playCount = new Array(videos.length).fill(0);
    document.playing = new Array(videos.length).fill(false);

    function vidNumber(e, prefix) {
        if (!prefix) {
            prefix = "carouselvid"; // hard coded default
        }
        var vidClass;
        for (c of e.srcElement.classList) {
            if (String(c).startsWith(prefix)) {
                // note that I've hard coded the prefix you have to use here, this could be a problem eventually
                vidClass = String(c);
                break;
            }
        }
        // great, now get the number
        var vidNum = parseInt(vidClass.match(/\d+/)[0]);
        return {'class':vidClass, 'number':vidNum};
    }

    function setEmbeddedData(prefix, number, value) {
        const name = prefix + parseInt(number).toString();
        Qualtrics.SurveyEngine.setEmbeddedData(name, value);
        return name;
    }

    function autoPlayObserverFunction(entries) {
        for (entry of entries) {
            if (entry.isIntersecting) {
                entry.target.play();
            } else {
                entry.target.pause();
            }
        }
    }

    function playCountEventFunction(e) {
        var vidClass, vidNum;
        vidClass = vidNumber(e);
        vidNum = vidClass.number;
        vidClass = vidClass.class;
        console.log(vidNum)
        console.log(vidClass)
        // now we have all that
        // do something to count the plays of this video
        document.playing[vidNum-1] = true;
        document.playCount[vidNum-1]++;
        document.prevPlayTime[vidNum-1] = Math.round(e.timeStamp);
        console.log("play event for video " + vidClass + " at timestamp " + Math.round(e.timeStamp) + " for current play count of " + document.playCount[vidNum-1]);
        setEmbeddedData('playCount', vidNum, document.playCount[vidNum-1]);
    }

    function pauseCountEventFunction(e) {
        var vidClass, vidNum;
        vidClass = vidNumber(e);
        vidNum = vidClass.number;
        vidClass = vidClass.class;
        console.log(vidNum)
        console.log(vidClass)        // now we have all that
        console.log("pause event for video " + vidClass + " at timestamp " + Math.round(e.timeStamp));
        var currPlayTime = Math.round(e.timeStamp) - document.prevPlayTime[vidNum-1];
        document.playing[vidNum-1] = false;
        document.playTime[vidNum-1] += currPlayTime;
        console.log("this play time for video " + vidNum + " was " + currPlayTime + " for a total of " + document.playTime[vidNum-1]);
        setEmbeddedData('playTime', vidNum, document.playTime[vidNum-1]);
        console.log(Qualtrics.SurveyEngine.getEmbeddedData('playTime' + vidNum));
    }

    const observer = new IntersectionObserver(autoPlayObserverFunction);
    // IntersectionObserver API works really well. Here's documentation
    // https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API

    for (video of videos) {
        observer.observe(video);
        video.addEventListener('play', playCountEventFunction);
        video.addEventListener('pause', pauseCountEventFunction);
        video.addEventListener('ended', pauseCountEventFunction);
        video.addEventListener('emptied', pauseCountEventFunction);
    }

});

Qualtrics.SurveyEngine.addOnPageSubmit(function()
{
	console.log("recording time for last video");
    const videos = document.querySelectorAll('video');
    const currTimeStamp = Math.round(new Event('null').timeStamp); // use a dummy event to be sure we are getting timestamp in the right format
    var currPlayTime;
    for (i=0; i<videos.length; i++) {
        if (document.playing[i]) {
            console.log('Video number ' + (i+1) + ' was playing, recording it as ended');
            //videos[i].pause();
            currPlayTime = currTimeStamp - document.prevPlayTime[i];
            document.playTime[i] += currPlayTime;
            Qualtrics.SurveyEngine.setEmbeddedData('playTime' + (i+1), document.playTime[i]);
        }
        console.log("playTime" + (i+1) + ": " + Qualtrics.SurveyEngine.getEmbeddedData('playTime' + (i+1)));
    }

});
