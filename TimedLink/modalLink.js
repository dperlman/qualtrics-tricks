Qualtrics.SurveyEngine.addOnload(function () {
  document.linkEventLog = [];
  document.linkEventTime = 0;
	
  Qualtrics.SurveyEngine.setEmbeddedData("linkEventLog", "");
  Qualtrics.SurveyEngine.setEmbeddedData("linkEventTime", 0);

  // Functions for handling the modal popupfor the frame

  function showFrame() {
    // this actually shows the requested article. Also updates the record of how many times it was shown.
    console.log("showFrame()");
    const returnablePage = new bootstrap.Modal(document.getElementById("myModalReturnable"), {});
    returnablePage.show();
	return returnablePage;
  }
	
  document.showFrame = showFrame;
	
  function hideFrame() {
	  document.frameModal.hide();
	  //document.nextB();
  }
  document.hideFrame = hideFrame;

  function launchLink() {
    // this is the function that is called by the user clicking the "read article" link.
    console.log("launchLink()");
    document.frameModal = showFrame();
	setTimeout("document.hideFrame(document.frameModal)", 20000);
  }
  document.launchLink = launchLink;
	
});


Qualtrics.SurveyEngine.addOnReady(function () {
  // need to do this per Stack Overflow
  document.body.appendChild(document.getElementById("myModalReturnable"));
  //this.hideNextButton();
  document.nextB = this.showNextButton;
});

Qualtrics.SurveyEngine.addOnPageSubmit(function () {
  
  // need to get rid of these, otherwise they linger after Qualtrics engine reloads the next block
  document.getElementById("myModalReturnable").remove();

});