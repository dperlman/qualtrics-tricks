Qualtrics.SurveyEngine.addOnload(function()
{
	/*Place your JavaScript here to run when the page loads*/

});

Qualtrics.SurveyEngine.addOnReady(function()
{
    function getMult(t) {
        // for label text t, use regex to get the multiplier value
        const m = String(t).match(/X=[\d]+/);
        if(m) {
            return Number(m[0].match(/\d+/)[0]);
        } else {
            return 1;
        }
    }
	function processDiv(d, i) {
        // d is the div, i is the index number starting at 1
        //console.log(d);
        d.style.transform = "translate(-25px,0px)";
        const counterID = 'b_lab_counterField' + i;
        const labelText = d.parentElement.querySelector('span').textContent;
        console.log(labelText);
        const mult = getMult(labelText); // this is a number
        console.log(mult);
        const inpOrig = d.querySelector('input');
        inpOrig.setAttribute('b_lab_localnum', i);
        var inp = inpOrig.cloneNode(true);
        inpOrig.after(inp);
        inp.setAttribute('multiplier', mult);
        inp.style.marginLeft = '15px';
        inp.id = counterID;
        console.log('processDiv() processed original input, created input:');
        console.log(inpOrig);
        console.log(inp);
    }

    function addHeader() {
        // first get the first li element
        var liFirst = document.querySelector('li.Selection');
        // get the position info of the first li div
        var d = liFirst.querySelector('div.SumInput');
        var dHead = document.createElement('div');
        d.insertBefore(dHead, d.firstChild);
        dHead.textContent = "Tokens Winnings";
        liFirst.parentElement.parentElement.style.overflowY = "visible";
        dHead.style.height = 0;
        dHead.style.position = "relative";
        dHead.style.top = "-20px";
    }

    function inputChangeCallback(e) {
        const val = Number(e.target.value.strip());
        console.log('change event triggered:');
        console.log(this);
        const i = Number(this.getAttribute('b_lab_localnum'));
        const counterID = 'b_lab_counterField' + i;
        const selector = 'li.Selection div.SumInput input#' + counterID;
        console.log(selector);
        const targInp = document.querySelector(selector);
        const mult = Number(targInp.getAttribute('multiplier'));
        console.log(mult);
        targInp.value = val * mult;

    }
    
	const l = document.querySelectorAll('li.Selection div.SumInput');

    for (i=1; i<=l.length; i++) {
        l[i-1].querySelector('input').addEventListener('change', inputChangeCallback);
        processDiv(l[i-1], i);
    }

    // now make the total field line up too
    const inpTot = document.querySelector('li.Total div.SumTotal input');
    inpTot.style.marginLeft = '-25px';

    // now add a header
    addHeader();

});

Qualtrics.SurveyEngine.addOnUnload(function()
{
	/*Place your JavaScript here to run when the page is unloaded*/

});