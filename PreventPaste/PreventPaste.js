Qualtrics.SurveyEngine.addOnload(function()
{
	/*Place your JavaScript here to run when the page loads*/

});

Qualtrics.SurveyEngine.addOnReady(function()
{
	// This simple code makes it so that any text input fields in the current question
	// will NOT accept copy-paste text.
	// It's specific to this question, it won't affect other questions on the page.
	var inputText = this.getChoiceContainer().querySelector('.InputText');
	console.log(inputText);
	inputText.addEventListener('paste', e => e.preventDefault());
	inputText.addEventListener('drop', e => e.preventDefault());
});

Qualtrics.SurveyEngine.addOnUnload(function()
{
	/*Place your JavaScript here to run when the page is unloaded*/

});
