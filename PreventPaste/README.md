# Qualtrics Tricks! Prevent Paste

## Introduction
This is simple code to prevent pasting into input text fields. It works for a single question and should not affect other questions on the same page.

## Instructions

1. Open the file "PreventPaste.js". You can do that directly on the web by clicking on it. This will let you see the contents of the file.
2. Select the entire contents of that file (not the whole web page!) and copy it (CMD-C or CTRL-C).
3. Go into your Qualtrics survey. For the question with a text input where you want to prevent pasting, click on that question, and then at the lower left, click "JavaScript".
4. In the window that opens, delete the placeholder text and replace it with the contents of the file you just copied.

Note that if you already have JavaScript in the question, then these instructions will, obviously, delete that. We assume that if you already have JavaScript in the question then you know what you're doing and you don't need these detailed instructions.

