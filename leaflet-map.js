Qualtrics.SurveyEngine.addOnload(function()
{
    // create this at the beginning so it's available globally throughout all these functions
    window.marker = null;
    // also get this from the embedded data so it's available globally
    var apiKey = Qualtrics.SurveyEngine.getEmbeddedData("apiKey");
	// also do this, because "this" doesn't work inside functions
	var questionContainer = this.questionContainer; 
	// and for convenience call this like this
	var myInputField = questionContainer.querySelector('.ChoiceStructure input.InputText');

	// Add the geocoding
	    var geocodeAddress = function(address) {
			console.log("geocoding for " + address);
			var apiUrl = "https://maps.googleapis.com/maps/api/geocode/json";
			var fullUrl = apiUrl + "?address=" + encodeURIComponent(address) + "&key=" + apiKey;

			// Make an HTTP request to the Geocoding API
			var xhr = new XMLHttpRequest();
			xhr.open("GET", fullUrl, true);
			xhr.onreadystatechange = function() {
				if (xhr.readyState === 4 && xhr.status === 200) {
					var response = JSON.parse(xhr.responseText);
					if (response.status === "OK") {
						var lat = response.results[0].geometry.location.lat;
						var lng = response.results[0].geometry.location.lng;

						// Here you can decide what to do with the latitude and longitude
						console.log("Latitude: " + lat + ", Longitude: " + lng);

						// For example, save them as embedded data in Qualtrics
						Qualtrics.SurveyEngine.setEmbeddedData("Latitude", lat);
						Qualtrics.SurveyEngine.setEmbeddedData("Longitude", lng);
					} else {
						Qualtrics.SurveyEngine.setEmbeddedData("Latitude", "not found");
						console.error("Geocoding failed: " + response.status);
					}
					// Update the value.
					gLat = Qualtrics.SurveyEngine.getEmbeddedData('Latitude');
					gLon = Qualtrics.SurveyEngine.getEmbeddedData('Longitude');
					map.setView([gLat, gLon]);
					// not needed here 
					//myInputField.setValue(gLat+','+gLon);
					
					console.log("About to add marker");
					if (window.marker != null) {						
						console.log("changing marker");						
						window.marker.setLatLng([gLat,gLon]);						
					} else {
						console.log("adding new marker");
						window.marker = L.marker([gLat, gLon]).addTo(map);
					}

				}
			};
			xhr.send();
    };
	
	document.getElementById('addressForm').addEventListener('submit', function(event) {
			event.preventDefault(); // Prevent the traditional form submission

			var address = document.getElementById('addressInput').value;
			console.log("Address entered:", address);
    
			geocodeAddress(address);
	});

	
    // Ensure the Leaflet library is loaded
    if (typeof L === 'undefined') return;

    // Initialize the map
    var map = L.map('mapid').setView([37.7749, -122.4194], 10); // Adjust zoom level as needed

    // Set up the tile layer
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        maxZoom: 18,
    }).addTo(map);
	
    // Listen for map click events
    map.on('click', function(e) {
        var lat = e.latlng.lat.toFixed(5); // Get latitude of clicked location
        var lng = e.latlng.lng.toFixed(5); // Get longitude of clicked location

        //alert('Clicked location: ' + lat + ', ' + lng); // Example alert - remove or replace as needed

        /* 
         * Now, you need to pass these values to Qualtrics.
         * Assuming you have hidden questions with Question IDs "QIDLat" and "QIDLng" for latitude and longitude respectively.
         */
         
        // Set the value of hidden questions
        Qualtrics.SurveyEngine.setEmbeddedData('Latitude', lat);
        Qualtrics.SurveyEngine.setEmbeddedData('Longitude', lng);
		
		gLat = Qualtrics.SurveyEngine.getEmbeddedData('Latitude');
		gLon = Qualtrics.SurveyEngine.getEmbeddedData('Longitude');
		
		console.log({gLat,gLon});
	    myInputField.setValue(gLat+','+gLon);
		
		if (window.marker != null) {						
			console.log("changing marker");						
			window.marker.setLatLng([gLat,gLon]);						
		} else {
			console.log("adding new marker");
			window.marker = L.marker([gLat, gLon]).addTo(map);
		}

        // Alternatively, if you need to directly manipulate the value of a question field (not recommended without a hidden field):
        //document.getElementById('QR~QIDLat').value = lat;
        //document.getElementById('QR~QIDLng').value = lng;
    });
});


Qualtrics.SurveyEngine.addOnReady(function()
{
	
});

Qualtrics.SurveyEngine.addOnUnload(function()
{
	/*Place your JavaScript here to run when the page is unloaded*/
		gLat = Qualtrics.SurveyEngine.getEmbeddedData('Latitude');
		gLon = Qualtrics.SurveyEngine.getEmbeddedData('Longitude');
	    myInputField.setValue(gLat+','+gLon);
			

});
