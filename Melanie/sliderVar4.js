Qualtrics.SurveyEngine.addOnload(function()
{
	/*Place your JavaScript here to run when the page loads*/
	Qualtrics.SurveyEngine.setEmbeddedData('firstPtrDnTime', '-1');
	Qualtrics.SurveyEngine.setEmbeddedData('firstPtrUpTime', '-1');
	Qualtrics.SurveyEngine.setEmbeddedData('lastPtrDnTime', '-1');
	Qualtrics.SurveyEngine.setEmbeddedData('lastPtrUpTime', '-1');

});

Qualtrics.SurveyEngine.addOnReady(function()
{
	// The right way to use this:
	// Question type is "slider".
	// Number of statements has to be 1 (one) only.
	// Number of scale points may be able to be anything but some values might not work. Test your choice.
	// Number of grid lines may be able to be anything but some values might not work. Test your choice.
	// Set "snap to grid lines" in Qualtrics per your preference. Unfortunately this can't be controlled by javascript.
	// Turn off "add labels" and "show value".
	// There may be unexpected interactions with some custom "skins".
	// In particular, clicking on the track doesn't seem to work right if I use a "wide" skin.
	// One question per page only! It will definitely fail with multiple items.

	// In your survey's Look and Feel -> Style -> Custom CSS click Edit and add:
	// div.labels-container ul.numbers {
	//	color: transparent;
	//}
	// This is necessary to keep the numbers from flashing briefly as the widget loads.
	// If you also want to remove the horizontal track bar, you can also add this to the custom CSS:

	//div.track.invisible {
	//	background-color: transparent !important;
	//	border: transparent !important;
	//}
	
	// Set up embedded data fields to save the reaction time data: firstPtrDnTime, firstPtrUpTime, lastPtrDnTime, lastPtrUpTime

	// Set which changes you want to make by setting embedded data fields.
	// hideTrack (true or false): hides the track
	// addAnchorDots, preventDrag, preventAllClicks, allowOnlyClicksOnAnchors (true or false): each according to its name.
	// There is no need to edit this javascript, although if you prefer to do it that way that's fine.
	// You probably want one of these four combos of the four embedded data fields listed above:
	// true, false, false, false: Click and drag both enabled
	// true, false, true, false: Drag only, no click
	// true, true, false, false: Click only, no drag
	// true, true, false, true: Click only on anchors, no drag, snap to grid
	// Note that all these work fine with snap to grid enabled *or* disabled in Qualtrics, except the last one.
	// The last one looks stupid if you don't do snap to grid.
	
	// This will also keep track of whether the user activated the slider by clicking or dragging. 
	// If you want to record that, make embedded data fields clickMove and dragMove.
	// Each of those will end up with a count of the corresponding action types.

	// set up the anchor labels we want. You can change this if you want but make sure the numbers line up!
	var anchorLabels = ["",
		"Strongly\ndisagree",
		"Disagree",
		"Somewhat\ndisagree",
		"Slightly\ndisagree",
		"Slightly\nagree",
		"Somewhat\nagree",
		"Agree",
		"Strongly\nagree"];

	// Get the whole skin, this is essentially the whole playing field
	var skin = document.querySelector('div.Skin');
	// Get the question container we are in. 
	// As long as we stay inside that, then whatever we do here won't affect other questions.
	var q = this.getQuestionContainer();
	// Figure out what type of question it is
	var inner = q.querySelector('div.Inner');
	var HBAR, HSLIDER = false;
	if (inner.classList.contains('HBAR')) {
		HBAR = true;
	} else if (inner.classList.contains('HSLIDER')) {
		HSLIDER = true;
	}


	// set up storage for the timing records
	q.firstPtrDnTime = parseInt(Qualtrics.SurveyEngine.getEmbeddedData('firstPtrDnTime'));
	q.firstPtrUpTime = parseInt(Qualtrics.SurveyEngine.getEmbeddedData('firstPtrUpTime'));
	q.lastPtrDnTime = parseInt(Qualtrics.SurveyEngine.getEmbeddedData('lastPtrDnTime'));
	q.lastPtrUpTime = parseInt(Qualtrics.SurveyEngine.getEmbeddedData('lastPtrUpTime'));
	console.log("Starting values of variables: firstPtrDnTime, firstPtrUpTime, lastPtrDnTime, lastPtrUpTime: " + 
		q.firstPtrDnTime + ", " + 
		q.firstPtrUpTime + ", " + 
		q.lastPtrDnTime + ", " + 
		q.lastPtrUpTime);

	// get the elements we need

	// these are all within the question
	var track = q.querySelector('div.track');
	var handle = q.querySelector('div.handle.selected');
	var trackContainer = q.querySelector('div.horizontalbar.ChoiceStructure');
	var labels = q.querySelectorAll('div.horizontalbar div.labels-container ul.labels li');
	var numbersContainer = q.querySelector('div.horizontalbar div.labels-container ul.numbers');
	var numbers = q.querySelectorAll('div.horizontalbar div.labels-container ul.numbers li');
	var nAnchors = numbers.length; // I think that's better than counting labels, they seem blank by default
	var n = nAnchors; //for convenience later, short variable name, longer one here is for documentation


	// hide the track if that is requested
	if (Qualtrics.SurveyEngine.getEmbeddedData('hideTrack') == 'true') {
		track.classList.add('invisible');
	}
	
	// deal with the anchor dots
	var trackHeight = Qualtrics.SurveyEngine.getEmbeddedData('trackHeight');
	if (!trackHeight) {trackHeight = "8px";}
	var trackBorder = Qualtrics.SurveyEngine.getEmbeddedData('trackBorder');
	if (!trackBorder) {trackBorder = '1px solid rgba(120,120,120,.7)';}
	var anchorDotSize = Qualtrics.SurveyEngine.getEmbeddedData('anchorDotSize');
	if (!anchorDotSize) {anchorDotSize = "15px";}
	var anchorDotBorder = Qualtrics.SurveyEngine.getEmbeddedData('anchorDotBorder');
	if (!anchorDotBorder) {anchorDotBorder = "4px solid black";}
	console.log('This is the track element:');
    console.log(track);

	var addAnchorDots = Qualtrics.SurveyEngine.getEmbeddedData('addAnchorDots');
	var preventDrag = Qualtrics.SurveyEngine.getEmbeddedData('preventDrag');
	var preventAllClicks = Qualtrics.SurveyEngine.getEmbeddedData('preventAllClicks');
	var allowOnlyClicksOnAnchors = Qualtrics.SurveyEngine.getEmbeddedData('allowOnlyClicksOnAnchors');
	// Set defaults if the user did not set the embedded data fields
	addAnchorDots = (addAnchorDots=='false')?false:true;
	preventDrag = (preventDrag=='true')?true:false;
	preventAllClicks = (preventAllClicks=='true')?true:false;
	allowOnlyClicksOnAnchors = (allowOnlyClicksOnAnchors=='true')?true:false;
	
	// note: if you allow only clicks on anchors, and there are no anchors, then obviously it won't work!



	trackContainer.style.overflow = 'visible';  ////NOTE TO SELF: TEST IF THIS IS STILL NECESSARY


	for (let i=0; i<n; i++) {
		// set the names
		numbers[i].innerText = anchorLabels[i];
		numbers[i].style = "color:black";
		numbers[i].style.width = "20px";
		numbers[i].style.overflow = 'visible';
		numbers[i].style.display = 'flex';
		numbers[i].style.textAlign = 'center';
		numbers[i].style.justifyContent = 'center';
	}
	//numbersContainer.style.tableLayout = 'fixed';
	numbersContainer.style = "justify-content: space-between; display: flex;"
	
	// Override skin settings to make this uniformly work, hopefully.
	track.style.height = trackHeight;
	track.style.backgroundColor = 'rgb(208,208,208)';
	track.style.border = trackBorder;
	track.style.boxSizing = 'border-box';
	var cssText = `
		width: ` + anchorDotSize + ` !important;
		height: ` + anchorDotSize + ` !important;
		background: rgb(150,150,150) !important;
		border-radius: 100% !important;
		border: none !important;
		position: absolute;`;
	console.log(cssText);
	handle.style.cssText = cssText;

	
	var handleWidth = parseFloat(getComputedStyle(handle).width);
	var trackWidth = parseFloat(getComputedStyle(track).width);

	// now that we have set the track and handle elements to the size and shape we want, we can get the offsets for the handle and anchors
	var offsets = getHandleOffset(track, anchorDotSize, anchorDotBorder);
	console.log(offsets);
	var handleOffset = offsets.handleOffset;
	var anchorOffset = offsets.anchorOffset;
	// set this one now, we won't be doing anything else with the handle
	handle.style.top = handleOffset;

	

	
	// the next bit is for making uniformly spaced anchor marks that match the labels
	if (addAnchorDots) {
		// temporarily make an anchor to test its size
		var testAnchor = handle.cloneNode();
		testAnchor.id = testAnchor.id + "_testAnchor";
		testAnchor.classList.add('mbhsCustomAnchor');
		testAnchor.classList.remove('handle');
		handle.after(testAnchor);
		// we have to style it otherwise the measurements are wrong
		testAnchor.style.cssText = `
				height: ` + anchorDotSize + `;
				width: ` + anchorDotSize + `;
				top: ` + anchorOffset + `;
				border: ` + anchorDotBorder + `;
				border-radius: 100%;
				position: absolute;
				`;
				//-moz-box-sizing: border-box;
    			//box-sizing: border-box;`;

		var anchorWidth = parseFloat(getComputedStyle(testAnchor).width) + 2*parseFloat(anchorDotBorder);
		testAnchor.remove();
		// ok now we have the size vars we need and the element is gone

		var widthDiff = (anchorWidth - handleWidth)/2;
		console.log('handle width is ' + handleWidth + ' and anchor width is ' + anchorWidth + ' widthDiff is ' + widthDiff);

		var spacing = (trackWidth - handleWidth)/(n-1);
		console.log('calculated spacing = ' + spacing);
	
		var a;
		for (let i=0; i<n; i++) {
			a = handle.cloneNode();
			a.id = a.id + "_anchor" + (i+1);
			a.classList.add('mbhsCustomAnchor');
			a.classList.remove('handle');
			handle.after(a);
			// now set all the other styles we need, that way this is all in here and we don't need any custom css
			a.style.cssText = `
				height: ` + anchorDotSize + `;
				width: ` + anchorDotSize + `;
				top: ` + anchorOffset + `;
				border: ` + anchorDotBorder + `;
				z-index: 499;
				border-radius: 100%;
				position: absolute;
				background: white;
				`;
				//-moz-box-sizing: border-box;
    			//box-sizing: border-box;`;
			// now we have to set the position after that because that reset all the css
			a.style.left = '' + (i * spacing - widthDiff) + 'px';
			//console.log(getComputedStyle(a).width);
			// this last bit makes sure that clicking is allowed on the anchor points.
			if (preventAllClicks) {
                a.style.pointerEvents = 'none';
            } else {
                a.style.pointerEvents = 'auto';
            }
		}
	}


	// This bit turns off dragging for the entire question area.
	// If there are more than one question on the screen at the same time this affects all of them.
	// This is the only place to turn off dragging that will cover the whole question area,
	// and also will be reset on the next page.
	if (preventDrag) {
		function mouseClickHandler(e) {
			//e = e || window.event;
			e.stopPropagation();
    		e.preventDefault();
    		e.stopImmediatePropagation();
    		return false;
		}
		skin.addEventListener("mousemove", mouseClickHandler, true);
	}

	// this bit turns off events for the track but turns them back on for the handle
	// which means click doesn't work but drag does work.
	if (preventAllClicks) {
		track.style.pointerEvents = 'none';
		handle.style.pointerEvents = 'auto';
	}
	
	// This is to ONLY allow clicks ON the anchor points and not in between
	if (allowOnlyClicksOnAnchors) {
		track.style.pointerEvents = 'none';
	}

	

	// This section is for determining whether the handle was clicked or dragged,
	// and logging the times and coordinates.
	var slider_ptr_down_event = function(event) {
		if (event.target.classList.contains('NextButton')) {
			return
		}
		q.mouseX = event.pageX;
		q.trackX = handle.style.left;
		// record timing
		q.lastPtrDnTime = event.timeStamp;
		if (q.firstPtrDnTime == -1) {
			q.firstPtrDnTime = event.timeStamp;
		}
		console.log('DOWN at ' + event.timeStamp);
	}

	var slider_ptr_up_event = function(event) {
		if (event.target.classList.contains('NextButton')) {
			return
		}
		// record timing
		q.lastPtrUpTime = event.timeStamp;
		if (q.firstPtrUpTime == -1) {
			q.firstPtrUpTime = event.timeStamp;
		}
		//console.log(handle.style.left);
		console.log('UP at ' + event.timeStamp);
		var mouse_change = parseInt(event.pageX) - parseInt(q.mouseX);
		var slider_change = parseInt(handle.style.left) - parseInt(q.trackX);
		console.log('Mouse was moved by ' + mouse_change)
		console.log('Slider was moved by ' + slider_change)
		var clickMove = parseInt(Qualtrics.SurveyEngine.getEmbeddedData('clickMove'));
		var dragMove = parseInt(Qualtrics.SurveyEngine.getEmbeddedData('dragMove'));
		console.log('Before: clickMove: ' + clickMove + ' dragMove: ' + dragMove);
		clickMove += (!mouse_change && slider_change)?1:0; // if mouse_change is zero, but the slider moved, it was a click move
		dragMove += (mouse_change && slider_change)?1:0; // if both numbers are not zero, it was a drag move
		console.log('After: clickMove: ' + clickMove + ' dragMove: ' + dragMove);
		Qualtrics.SurveyEngine.setEmbeddedData('clickMove', clickMove);
		Qualtrics.SurveyEngine.setEmbeddedData('dragMove', dragMove);
	}

	// here is where I actually add the eventListeners.
	skin.addEventListener('pointerdown', slider_ptr_down_event, true);
	//skin.addEventListener('pointerup', slider_ptr_up_event, false);
	window.addEventListener('pointerup', slider_ptr_up_event, false);
	

	function getHandleOffset(track, anchorDotSize, anchorDotBorder) {
		console.log('getHandleOffset')
		console.log(track);
		console.log(track.style);
		var trackBarOuter = track.closest('.BarOuter');
		var trackContainerHeight = trackBarOuter.getHeight();
		console.log('trackContainerHeight: ' + trackContainerHeight);
		var trackHeight = getComputedStyle(track).height;
		console.log(trackHeight);
		var trackBorder = getComputedStyle(track).borderWidth;
		console.log(trackBorder);
		var thNum = parseFloat(trackHeight.match(/\d+/)[0]);
		//var tchNum = parseFloat(trackContainerHeight.match(/\d+/)[0]);
		var tchNum = parseFloat(trackContainerHeight);
		var tbNum = parseFloat(trackBorder.match(/\d+/)[0]);
		var adsNum = parseFloat(anchorDotSize.match(/\d+/)[0]);
		var adbNum = parseFloat(anchorDotBorder.match(/\d+/)[0]);
		var thStr = trackHeight.match(/\D+/)[0];
		var adsStr = anchorDotSize.match(/\D+/)[0];
		console.log(thNum, tchNum, tbNum, adsNum, adbNum, thStr, adsStr);
		var handleOffset = -tbNum + thNum/2 - adsNum/2;
		var anchorOffset = handleOffset - adbNum;
		//var anchorOffset = handleOffset - tbNum;
		var retObj = {};
		retObj.handleOffset = handleOffset.toString() + thStr;
		retObj.anchorOffset = anchorOffset.toString() + thStr;
		console.log(retObj);
		console.log(`getHandleOffset found above offsets from trackHeight ` + trackHeight + ` anchor ` + anchorDotSize + ` border ` + anchorDotBorder + ` track border ` + trackBorder);
		return retObj;
	}
});

Qualtrics.SurveyEngine.addOnPageSubmit(function()
{
	var q = this.getQuestionContainer();
	// save these
	Qualtrics.SurveyEngine.setEmbeddedData('firstPtrDnTime', q.firstPtrDnTime);
	Qualtrics.SurveyEngine.setEmbeddedData('firstPtrUpTime', q.firstPtrUpTime);
	Qualtrics.SurveyEngine.setEmbeddedData('lastPtrDnTime', q.lastPtrDnTime);
	Qualtrics.SurveyEngine.setEmbeddedData('lastPtrUpTime', q.lastPtrUpTime);
	console.log("Recorded pointer up and down times: firstPtrDnTime, firstPtrUpTime, lastPtrDnTime, lastPtrUpTime: " + 
		q.firstPtrDnTime + ", " + 
		q.firstPtrUpTime + ", " + 
		q.lastPtrDnTime + ", " + 
		q.lastPtrUpTime);

});