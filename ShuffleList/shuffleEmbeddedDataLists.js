Qualtrics.SurveyEngine.addOnload(function()
{
  /*Place your JavaScript here to run when the page loads*/

});

Qualtrics.SurveyEngine.addOnReady(function()
{
  // This code lets you shuffle "arrays" of embedded data fields.
  // Of course there is no such thing as an array of embedded data,
  // but if you have fields with all the same prefix followed by a number,
  // that's what this uses.
  // Note that you want them to be like Field1, not Field01.

  // The way to use this is that you make another embedded data field called 'ShuffleFieldPrefixList'.
  // This is a comma-delimited list of the prefixes (names without numbers) of the fields you want this to shuffle.
  // "Arrays" can't have more than 99 elements. I had to draw the line somewhere.

  function shuffleArray(array) {
      for (let i = array.length - 1; i >= 1; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [array[i], array[j]] = [array[j], array[i]];
      }
  }
  
  function writeEmbeddedDataList(prefix, array, indexStart) {
    for (let i=0; i<array.length; i++) {
      var eDataName = String(prefix) + (i+indexStart);
      Qualtrics.SurveyEngine.setEmbeddedData(eDataName, array[i]);
      console.log('ShuffleList/writeEmbeddedDataList set ' + eDataName + ' = ' + array[i]);
    }
  }
    
  const maxInd = 100; // normally 100
  var shuffleList = Qualtrics.SurveyEngine.getEmbeddedData('ShuffleFieldPrefixList');
  shuffleList = shuffleList.split(',');
  
  for (let p of shuffleList) {
    var actualCount = 0;
    var values = [];
    var startIndex = 0;
    for (let i=0; i<maxInd; i++) {
      // note this means the highest index we are willing to check is 99
      // it will try zero and skip it if it's not available
      var varName = String(p) + i; 
      var v = Qualtrics.SurveyEngine.getEmbeddedData(varName);
      console.log(varName + ": " + v);
      if (i==0 && v==null) {
        startIndex = 1;
        console.log('ShuffleList start index is not zero for prefix ' + p + '(this is fine, just letting you know)');
        continue;
      } else if (i>0 && v==null) {
        console.log('ShuffleList ran out of fields at ' + varName);
        break;
      }
      // now handle an actual value
      values.push(v);
      actualCount++;
      console.log('ShuffleList: ' + varName + ' = ' + v);	
    }
    console.log('ShuffleList did ' + actualCount + ' values for prefix ' + p);
    shuffleArray(values); // this does it in-place
    writeEmbeddedDataList(p, values, startIndex);
  }
  console.log('ShuffleList shuffled ' + shuffleList.length + ' lists!');

});

Qualtrics.SurveyEngine.addOnUnload(function()
{
  /*Place your JavaScript here to run when the page is unloaded*/

});