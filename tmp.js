Qualtrics.SurveyEngine.addOnload(function()
{
	document.qSGSBBlab_PID = Qualtrics.SurveyEngine.getEmbeddedData('participantId');
	document.qSGSBBlab_userClickEvents = [];
	document.whichPage = Qualtrics.SurveyEngine.getEmbeddedData('whichPage');
	
	const spinSpeed = Qualtrics.SurveyEngine.getEmbeddedData('spinSpeed');
	const waitTimeMillisec = Qualtrics.SurveyEngine.getEmbeddedData('WaitTimeMillisec');

	document.qSGSBBlab_saveEvent = function(evnt) {
		console.log('running document.qSGSBBlab_saveEvent() with event:');
		console.log(evnt.type);
		console.log(evnt.target);
		var link;
		if (evnt.target) {
			if (evnt.target.closest) {
				//link = evnt.target.closest('a').href.toString().replace('http://','').replace('https://','').replace('/','');
				link = evnt.target.closest('a').href.toString();
				console.log(link);
			}
		}
		// link might be empty
		var eStore = {pid:document.qSGSBBlab_PID,
					  page:document.whichPage,
					  visibility:document.visibilityState,
					  nowTime:(new Date()),
					  eventTime:evnt.timeStamp,
					  eventType:evnt.type,
					  spinSpeed:spinSpeed,
					  waitTimeMillisec:waitTimeMillisec,
					  pointerType:'',
					  x:'',
					  y:'',
					  link:'',
					  touchScreen:'',
					  userAgent:'',
					  postalCode:'${loc://PostalCode}',
					  sessionId:'${e://Field/ResponseID}',
					  ipAddress:'${loc://IPAddress}'
				};	
		if ('userAgent' in evnt) {
			eStore.userAgent = evnt.userAgent;
		} else if (typeof evnt == 'string') {
    		eStore.eventType = evnt;
		} else if (evnt.type == 'visibilitychange') {
			eStore.eventType = 'visibilitychange.' + document.visibilityState;
		} else if (evnt.type == 'click') {
			eStore.pointerType = evnt.pointerType,
			eStore.x = evnt.x;
			eStore.y = evnt.y;
			if (link) {
				eStore.link = link;
			}
		} else if (evnt.type.startsWith('touchScreen')) {
			eStore.eventType = 'touchScreenTest';
			eStore.touchScreen = evnt.type;
		}
			
		document.qSGSBBlab_userClickEvents.push(eStore);
	}
	
	document.qSGSBBlab_remoteLogSavedEvents = function() {
		var url =Qualtrics.SurveyEngine.getEmbeddedData('clickLogUrl');
		var report = document.qSGSBBlab_userClickEvents;
		console.log('running document.qSGSBBlab_remoteLogSavedEvents() with data:');
		console.log(JSON.stringify(report));
		navigator.sendBeacon(url, JSON.stringify(report));
		document.qSGSBBlab_userClickEvents = [];
	}
	
	document.qSGSBBlab_windowUnloadTasks = function() {
		document.qSGSBBlab_saveEvent(new Event('windowUnload'));
		document.qSGSBBlab_remoteLogSavedEvents();
		console.log('windowUnloadTasks: about to return placeholder string');
		//return '';
	}
		
	document.addEventListener("click", document.qSGSBBlab_saveEvent);
	document.addEventListener("visibilitychange", document.qSGSBBlab_saveEvent);
	
});


Qualtrics.SurveyEngine.addOnReady(function() {

	this.hideNextButton();
	const delay=Qualtrics.SurveyEngine.getEmbeddedData('WaitTimeMillisec');
	document.qSGSBBlab_saveEvent(new Event('pageStart.' + document.visibilityState));
	window.onunload = document.qSGSBBlab_windowUnloadTasks;

	if (document.whichPage == 'waitCursor') {
		// detect and save touch screen status
		if (('ontouchstart' in window) || 
			(navigator.maxTouchPoints > 0) ||
			(navigator.msMaxTouchPoints > 0)) {
			var touchScreen = new Event('touchScreenYes');
		} else {
			var touchScreen = new Event('touchScreenNo');
		}
		document.qSGSBBlab_saveEvent(touchScreen); // records touch screen status
		// store user agent
		var userAgentEvent = new Event('userAgentCheck');
		userAgentEvent.userAgent = navigator.userAgent;
		document.qSGSBBlab_saveEvent(userAgentEvent); 
		//window.onbeforeunload = document.qSGSBBlab_windowUnloadTasks;
   		setTimeout(function() {
    		//window.onbeforeunload = null;
    		window.onunload = null;
			document.qSGSBBlab_saveEvent(new Event('timerFinished'));
			jQuery("#NextButton").click();
		}, delay);
	} else { // not the wait cursor page, presumably the sunscreen page
   		setTimeout(function() {
    		//window.onbeforeunload = null;
    		window.onunload = null;
			document.qSGSBBlab_saveEvent(new Event('subsequentPageSendData'));
			document.qSGSBBlab_remoteLogSavedEvents(); // send the data after a little while. Just so we don't have to wait forever
		}, delay); // or should the data send delay be different?		
	}
});



Qualtrics.SurveyEngine.addOnPageSubmit(function()
{
	document.qSGSBBlab_saveEvent(new Event('properPageSubmit.' + document.visibilityState));
	document.qSGSBBlab_remoteLogSavedEvents();
	document.removeEventListener("click", document.qSGSBBlab_saveEvent);
	document.removeEventListener("visibilitychange", document.qSGSBBlab_saveEvent);
});