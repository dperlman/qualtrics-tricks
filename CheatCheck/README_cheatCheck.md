# Qualtrics Tricks: Cheating Checker

## Introduction
This system allows you to track and record any time that survey participants spend changing tabs, or looking at other apps, etc. You might care about this if you are asking people questions about things that are supposed to test their own knowledge or opinions or guesses or memory and you're worried they might look things up online. Or maybe you just want to know if you have their undivided attention.

This javascript code uses the "blur" and "focus" events for the Window element. This has been part of the HTML standard for many years and appears to be supported by all browsers. It seems like this should work more or less universally on desktop and mobile on all platforms.[^1]

The section "Fine Points to be aware of" contains critical information, however it might be confusing if you read it before you have familiarized yourself with how the setup goes. I recommend that you start the process of setting this up in your survey, and then go back and read this critical information to see if it applies to you. It will be more clear whether it applies to you after you have done your own setup.

The overall way this works is that you set up embedded data fields for whatever number of questions you want to monitor, then you add the javascript to each of those questions. The fields then record how much time and how many times they spent away from the Qualtrics page for each question. 

Note that it is not possible for us to know what they were doing or where they went. Browsers won't allow a web page to get that information because that would be a security problem. The only way to get that information would involve installing a browser plugin or some other kind of local software on the partcipant's own computer, which they would have to agree to.

This code is intended to be as close to plug-and-play as possible. Qualtrics requires all embedded data fields to be declared in "embedded data" blocks in the Survey Flow, so it's not possible to do this entirely inside the javascript, you will need to manually create the necessary fields. The instructions provided here will hopefully make it straightforward and unambiguous.

## Instructions
- [Setting up embedded data fields](#embedded-data)
- [Setting up question javascript](#javascript)
- [Interpreting the data](#data-interpretation)
- [Fine Points to be aware of](#fine-points)


<a id="embedded-data"></a>
### Setting up embedded data fields
1. You will need to know the number of questions you want to monitor individually. For example, if there are 4 questions where you want to monitor people's cheating behavior, then the number is 4. In the text below I will refer to this number as NQ for "number of questions".
2. Go into the Survey Flow part of Qualtrics.
![Image of how to access Survey Flow in Qualtrics](images/SurveyFlow.png){width=500}
3. Add a new block near the beginning. Choose type "Embedded Data" when it says "What do you want to add?"
4. Create the following embedded data fields. Only the first one gets an initial value:
    * CurrQLogNum = 0
    * CheatTime1 through NQ
    * CheatEventLog1 through NQ
5. Double check to make sure the embedded data block is at or near the beginning of the survey flow. It must be before any blocks where you want to use the code.
6. Click Apply to save. Qualtrics will not save the block you created unless you click Apply!
![Image of Apply button in Qualtrics Survey Flow](images/survey-flow-1.png){width=500}

<a id="javascript"></a>
### Setting up the javascript
1. Go back to the Survey Builder
![Image of how to access Survey Builder in Qualtrics](images/survey-builder-1.png){width=500}
2. Repeat the following steps for each question you want to monitor:



<a id="data-interpretation"></a>
### Interpreting the data
Second item content goes here


<a id="fine-points"></a>
### Fine Points to be aware of
* Since this monitoring system works on a page, if you have two qualtrics questions that show up on a page together, it will monitor both of them. So, don't add this code separately to two questions that share a page. It probably won't crash, but it's a waste of effort and it won't give you any additional information.
* It's also possible to intentionally monitor a whole series of items together. For example, if you want to record cheating events for your whole survey, and you're not interested in keeping track of which question it happened on, you can put the first two parts of the javascript code (the addOnLoad and addOnReady parts) on the first question, and then put the addOnPageSubmit on the last question. Then NQ will just be 1, so only make 1 of each embedded data field.
* The only scenario that you can expect this to work reliably for is when users switch to a different tab, or window, or app. If the user quits the browser or especially if they force-quit, or if the battery dies on their device, this probably won't record anything.


[^1]: Hypothetical future platforms might not work with this. For example it's easy to imagine that this might not work the way you expect it to in a VR environment.

