// For a Qualtrics "form field" question, you might want to adjust the size of all the form inputs all at once.
// For example, you want them to all be the same size, more precisely than you could do by hand in the Survey Builder.
// This simple code will do that. 
// It should also point out how you would do more complex changes like setting each one individually.
// It goes in the addOnReady() section in Qualtrics, because the fields have to exist already when this runs.
// Replace the '350px' with whatever size you want.

document.querySelectorAll('td.ControlContainer input.Long.InputText').forEach(x => x.style = 'width:350px');