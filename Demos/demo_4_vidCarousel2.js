// The challenge that this file is meant to solve was showing phone-format (portrait) videos
// in a Bootstrap carousel slider, with good aspect ratio formatting,
// smart autoplay (that takes the carousel into account),
// and recording play time of each video.
// For this version the videos are hosted in an S3 bucket with Cloudfront cacheing.
// They must already be in portrait aspect ratio, I am not including code here to crop them, although that is also possible.
// The particular challenge of this new version is how to get it to semi-autoplay with sound on mobile.
// I had it working perfectly on desktop but mobile restricts autoplay to silent.
// What we are trying to do here is to click once to start, then it's autoplay from there.

Qualtrics.SurveyEngine.addOnload(function () {

});

Qualtrics.SurveyEngine.addOnReady(function () {  
  function vidNumber(e, prefix) {
    let target;
    if (!prefix) {
      prefix = "c1v"; // hard coded default, same as set above (yeah this is bad coding practice)
    }
    var vidClass;
    if ("vidNum" in e) {
      // if this is a fake event that we made for internal sync purposes, just use the number directly
      // console.log(
      //   "vidNumber() received a dummy event that had its own vidNum property, using that : " +
      //     e.vidNum
      // );
      return { class: null, vidNum: e.vidNum };
    }
    if ("element" in e) {
      target = e.element;
    } else if ("target" in e) {
      target = e.target;
    } else if ("srcElement" in e) {
      target = e.srcElement;
    } else if ("classList" in e) {
      target = e;
    } else {
      console.log(
        "vidNumber(): passed element/event does not have element or target or srcElement or classList properties, fail!!!"
      );
      console.log(e);
      return { class: null, vidNum: null };
    }
    // note that I've hard coded the prefix you have to use here, this could be a problem eventually
    if ("id" in target && target.id.startsWith(prefix)) {
      vidClass = String(target.id);
    } else {
      for (c of target.classList) {
        if (String(c).startsWith(prefix)) {
          vidClass = String(c);
          break;
        }
      }
    }
    vidSuffix = vidClass.slice(prefix.length);
    const fullName = getFullName(target);
    //console.log('vidNumber is looking at element with full name: ' + fullName);
    // great, now get the number
    var vidNum = parseInt(vidSuffix.match(/\d+/)[0]);
    // console.log(
    //   "vidNumber() found vidNum: " + vidNum + " for element: " + fullName
    // );
    return { class: fullName, vidNum: vidNum };
  }


  function getFullName(element) {
    const tag = element.localName;
    const id = element.identify();
    const classes = [...element.classList].join(".");
    const fullName = tag + "#" + id + "." + classes;
    // console.log("getFullName() got a full name: " + fullName);
    return fullName;
  }

  function playEventHandler(e) {
    var vidClass, vidNum;
    vidClass = vidNumber(this);
    vidNum = vidClass.vidNum;
    vidClass = vidClass.class;
    //console.log(vidNum);
    //console.log(vidClass);
    document.isPlaying[vidNum - 1] = true;
    document.playCount[vidNum - 1]++;
    document.prevPlayTime[vidNum - 1] = e.timeStamp;
    console.log(
      "play event for video " +
        vidClass +
        " (vidNum " +
        vidNum +
        ") " +
        " at timestamp " +
        e.timeStamp.toFixed(0) +
        " for current play count of " +
        document.playCount[vidNum - 1]
    );
    document.dp_setED("playCount", document.playCount[vidNum - 1], vidNum);
    document.playPauseWaitTimes[vidNum - 1] += ("+play@" + e.timeStamp.toFixed(0));
  }

  function pauseEventHandler(e) {
    console.log("Pause event handler triggered!");
    var vidClass, vidNum;
    vidClass = vidNumber(this);
    vidNum = vidClass.vidNum;
    vidClass = vidClass.class;
    //console.log(vidNum);
    //console.log(vidClass)
    console.log(
      "Pause event is for video " +
        vidClass +
        " (vidNum " +
        vidNum +
        ") " +
        " at timestamp " +
        e.timeStamp.toFixed(0)
    );
    // track the intended play time (user initiated)
    var currPlayTime =
      e.timeStamp - document.prevPlayTime[vidNum - 1];
    document.isPlaying[vidNum - 1] = false;
    document.playTime[vidNum - 1] += currPlayTime;
    document.playTimes[vidNum - 1] += ("+" + currPlayTime.toFixed(0));
    
    console.log(
      "this play time for video " +
        vidClass +
        " (vidNum " +
        vidNum +
        ") " +
        " was " +
        currPlayTime.toFixed(0) +
        " for a total of " +
        document.playTime[vidNum - 1].toFixed(0)
    );
    document.dp_setED("playTime", document.playTime[vidNum - 1].toFixed(0), vidNum);
    document.dp_setED("playTimes", document.playTimes[vidNum - 1], vidNum);

    // track the actual playing time (eg not buffering)
    if (document.prevPlayingTime[vidNum - 1] > 0) {
      var currPlayingTime =
        e.timeStamp - document.prevPlayingTime[vidNum - 1];
      document.playingTime[vidNum - 1] += currPlayingTime;
    } else {
      // this means that we got a wait event before ever logging any currPlayingTime.
      // This often happens at the very beginning; it waits for a few ms before starting the first time.
      var currPlayingTime = 0;
    }
    document.playPauseWaitTimes[vidNum - 1] += ("+paus@" + e.timeStamp.toFixed(0));
    console.log(
      "this playING time for video  " +
        vidClass +
        " (vidNum " +
        vidNum +
        ") " +
        " was at pause " +
        currPlayingTime.toFixed(0) +
        " for a total of " +
        document.playingTime[vidNum - 1].toFixed(0)
    );
    document.dp_setED("playingTime", document.playingTime[vidNum - 1].toFixed(0), vidNum);
    document.dp_setED("playPauseWaitTimes", document.playPauseWaitTimes[vidNum - 1], vidNum);

  }



  function playingEventHandler(e) {
    var vidClass, vidNum;
    vidClass = vidNumber(this);
    vidNum = vidClass.vidNum;
    vidClass = vidClass.class;
    document.prevPlayingTime[vidNum - 1] = e.timeStamp;
    console.log(
      "playing event for video " +
        vidClass +
        " (vidNum " +
        vidNum +
        ") " +
        " at timestamp " +
        e.timeStamp.toFixed(0)
    );
    document.playPauseWaitTimes[vidNum - 1] += ("+plng@" + e.timeStamp.toFixed(0));
  }


  function waitingEventHandler(e) {
    var vidClass, vidNum;
    vidClass = vidNumber(this);
    vidNum = vidClass.vidNum;
    vidClass = vidClass.class;
    //document.prevPlayingTime[vidNum - 1] = window.performance.now();
    console.log(
      "waiting event for video " +
        vidClass +
        " (vidNum " +
        vidNum +
        ") " +
        " at timestamp " +
        e.timeStamp.toFixed(0)
    );
    // track the actual playing time (eg not buffering)
    if (document.prevPlayingTime[vidNum - 1] > 0) {
      var currPlayingTime =
        e.timeStamp - document.prevPlayingTime[vidNum - 1];
      document.playingTime[vidNum - 1] += currPlayingTime;
    } else {
      // this means that we got a wait event before ever logging any currPlayingTime.
      // This often happens at the very beginning; it waits for a few ms before starting the first time.
      var currPlayingTime = 0;
    }
    document.playPauseWaitTimes[vidNum - 1] += ("+wait@" + e.timeStamp.toFixed(0));
    console.log(
      "this playING time for video " +
        vidClass +
        " (vidNum " +
        vidNum +
        ") " +
        " was at buffering " +
        currPlayingTime.toFixed(0) +
        " for a total of " +
        document.playingTime[vidNum - 1].toFixed(0)
    );
    document.dp_setED("playingTime", document.playingTime[vidNum - 1].toFixed(0), vidNum);
    document.dp_setED("playPauseWaitTimes", document.playPauseWaitTimes[vidNum - 1], vidNum);
  }


  function swiperSlideChangeHandler(swiper) {
    // the swiper event API doesn't share the event, just the swiper, so we have to get timestamp ourselves
    const timeStamp = window.performance.now();
    const numVids = document.videos.length;
    var playPauseState = "";
    var oldPlayPauseState = "";
    var onePlayPauseState;
    var whichStarts = 0;
    var active = swiper.realIndex;
    var previous = swiper.previousIndex;
    for (i = 0; i < numVids; i++) {
      // log initial play pause states
      if (document.videos[i].readyState < 2) {
        // this means it's not ready to play at all
        onePlayPauseState = 'W';
      } else if (document.videos[i].paused) {
        // this means it's ready, but it's paused
        onePlayPauseState = '0';
      } else {
        // presume if we get here it's playing
        onePlayPauseState = '1';
      }
      oldPlayPauseState += onePlayPauseState;
      if (i == swiper.realIndex) {
        if (document.resetOnReturn) {
          document.videos[i].currentTime = 0;
        }
        document.videos[i].play();
        //document.playCount[i] += 1;
        playPauseState += '1';
        whichStarts = i+1;
      } else {
        // pause each one that's not showing
        document.videos[i].pause();
        playPauseState += '0';
      }
    }
    // now also log the transition
    // first figure out if there was a swipe
    var swipeEndTimeStamp = null;
    var swipeDist = null;
    var swipeText = "";
    //var navClickTimeStamp = null;
    //var navClickDir = null;
    if (document.touches.length > 0) {
      // more than one touches have been logged by the swiper
      // only one, that's not a swipe, so we start with more than one
      swipeEndTimeStamp = document.touches.slice(-1)[0]['timeStamp'];
      swipeDist = document.touches.slice(-1)[0]['pageX'] - document.touches[0]['pageX'];
      swipeText = "swipe" + swipeDist.toFixed(0) + "@" + swipeEndTimeStamp.toFixed(0);
      // debugging
      if (isNaN(swipeDist)) {
        console.log('swiperSlideChangeHandler() got swipeDist NaN:');
        console.log(document.touches);
        if (!Qualtrics.SurveyEngine.getEmbeddedData('swipeDebug1')) {
          Qualtrics.SurveyEngine.setEmbeddedData('swipeDebug1', JSON.stringify(document.touches));
        }
      }
    }
    document.touches = []; // clear this out no matter what
    var currLog = Qualtrics.SurveyEngine.getEmbeddedData("SlideEventLog");
    var newLog = "|" +
      document.navClicks + 
      swipeText +
      "transitionFrom:previous:" + previous + ":" + oldPlayPauseState + "->active:" + active + ":" + playPauseState + "@" + timeStamp.toFixed(0);
    currLog += newLog;
    console.log("swiperSlideChangeHandler(): adding slide change event to log:");
    console.log(newLog)
    Qualtrics.SurveyEngine.setEmbeddedData("SlideEventLog", currLog);
    document.navClicks = "";
  }

  function swiperNavButtonHandler(e) {
    //console.log(e.target);
    //e.preventDefault();
    var dir;
    if (String(e.target.className).toLowerCase().includes('next')) {
      dir = 'next';
    } else if (String(e.target.className).toLowerCase().includes('prev')) {
      dir = 'prev';
    } else {
      dir = 'unkn';
    }
    var newLog = dir + "_btn@" + e.timeStamp.toFixed(0);
    document.navClicks += newLog;
    // now log the transition
    var currLog = Qualtrics.SurveyEngine.getEmbeddedData("NavButtonLog");
    currLog += ("|" + newLog);
    console.log("swiperNavButtonLogger(): adding nav button event to log:");
    console.log("|" + newLog)
    Qualtrics.SurveyEngine.setEmbeddedData("NavButtonLog", currLog);
  }

  function swiperTouchMoveHandler(swiper, e) {
    // record all of them so we know what the user was messing around with between slides
    //var prevTouch = document.touches.slice(-1)[0];
    document.touches.push({pageX:e.pageX, timeStamp:e.timeStamp})
    // now log so we can see everything
    var newLog = "tM" + e.pageX + "@" + e.timeStamp.toFixed(0);
    var currLog = Qualtrics.SurveyEngine.getEmbeddedData("TouchMoveLog");
    currLog += ("|" + newLog);
    console.log("swiperTouchMoveHandler(): adding touchMove event to log:");
    console.log("|" + newLog)
    Qualtrics.SurveyEngine.setEmbeddedData("TouchMoveLog", currLog);

  }


  function toggleControls(video) {
    if (video.hasAttribute("controls")) {
        video.removeAttribute("controls")   
    } else {
        video.setAttribute("controls","controls")   
    }
  }

  function isVideoPlaying(video) {
    return !!(video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2);
  }


  function checkVisibility(elem) {
    // element.checkVisibility only works in the very newest Safari so we can't use it. Ugh.
    // so this is a workaround
    return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length || (window.getComputedStyle(elem).visibility == "hidden"));
  };

  function finalCleanup(e) {
    // this goes in an event listener for the next button
    console.log("Next button was clicked; recording time for last video.");
    for (i = 0; i < document.videos.length; i++) {
      if (document.isPlaying[i]) {
        // track the user-initiated playing time 
        console.log(
          "Video number " + (i + 1) + " was playing, recording it as ended"
        );
        var currPlayTime = e.timeStamp - document.prevPlayTime[i];
        document.playTime[i] += currPlayTime;
        document.playTimes[i] += ("+" + currPlayTime.toFixed(0));
        document.dp_setED("playTime", document.playTime[i].toFixed(0), i + 1);
        document.dp_setED("playTimes", document.playTimes[i], i + 1);
        document.isPlaying[i] = false;
        //document.videos[i].pause();
        // track the actual playing time (eg not buffering)
        if (document.prevPlayingTime[i] > 0) {
          var currPlayingTime =
            e.timeStamp - document.prevPlayingTime[i];
          document.playingTime[i] += currPlayingTime;
        } else {
          // this means that we got a wait event before ever logging any currPlayingTime.
          // This often happens at the very beginning; it waits for a few ms before starting the first time.
          var currPlayingTime = 0;
        }
        document.playPauseWaitTimes[i] += ("+nxtb@" + e.timeStamp.toFixed(0));
        document.dp_setED("playingTime", document.playingTime[i].toFixed(0), i+1);
        document.dp_setED("playPauseWaitTimes", document.playPauseWaitTimes[i], i+1);
      }
      console.log(
        "playTime" +
          (i + 1) +
          ": " +
          Qualtrics.SurveyEngine.getEmbeddedData("playTime" + (i + 1))
      ); // this still might not work due to delay in pause handler.
    }
    // now at the end, log the next/prev button clicks

  }

  ///////////////////////////////////////////////////////////////////////////////
  // Finished with function definitions, now act.

  // first initialize the swiper library
  // but first first, we add CSS to resize it to the now-known size of window
  // couldn't put it in the static HTML because we don't know the size of the window in advance
  // might need to polymorph this so this doesn't happen in mobile

  // find out if we want to reset them when moved away and then back
  const resetOnReturn = Qualtrics.SurveyEngine.getEmbeddedData("resetOnReturn");
  document.resetOnReturn =
    String(resetOnReturn).toLowerCase() == "false" ? false : true;
    // default is true if it's anything other than explicitly "false" in the embedded data
  // now find out if we are on mobile, at least according to Qualtrics
  const isMobile = Qualtrics.SurveyEngine.getEmbeddedData("IsMobile");
  document.isMobile = 
    String(isMobile).toLowerCase() == "false" ? false : true;
    // default is true if it's anything other than explicitly "false" in the embedded data

    
  var videoHeight, videoWidth;
  if (document.isMobile) {
    videoHeight = window.innerHeight;
    videoWidth = window.innerWidth;
  } else {
    videoHeight = 480;
    videoWidth = 270;
  }
  // need to deal with possible problems here
  if (videoWidth >= videoHeight) {
    videoWidth = videoHeight * 270 / 480
  }
  
  const swiperStyle = `
    .swiper {
      width: ` +  videoWidth + `px;
      height: ` + videoHeight + `px;
      }`;
  var styleSheet = document.createElement("style");
  styleSheet.textContent = swiperStyle;
  document.head.appendChild(styleSheet);
  console.log('Set new stylesheet:');
  console.log(swiperStyle);

  const swiper = new Swiper(".swiper", {
    direction: "horizontal",
    loop: false,

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    }
  });



  // Set a handler on the next button to save the last video.
  // have to do this because it never ended up working from the addOnPageSubmit section.
  // my testing indicated that the 5 ms delay in the player was long enough that setEmbeddedData didn't work anymore.
  document.getElementById("NextButton").addEventListener("click", finalCleanup);

  // now get all the video players and their associated iframes and set up global storage for our purposes
  document.videos = [
    ...document.querySelectorAll("video.video-js.vjs-default-skin"),
  ];
  // console.log(document.videos);
  const numVids = document.videos.length;

  // start the first one playing! hopefully this works
  // not sure why I'm checking visibility here? what was I thinking?
  //if (document.videos[0].checkVisibility()) {
  if (checkVisibility(document.videos[0])) {
    document.videos[0].play();
  }

  // document.players = new Array(numVids);
  // for (let i=0; i<numVids; i++) {
  //     let frame = document.videos[i];
  //     let numberedId = 'c1v' + (i+1).toString();
  //     let player = document.querySelector('video#' + numberedId);
  //     console.log(player);
  //     document.players[i] = player;
  //     document.dp_setED('playCount', 0, i+1);
  //     document.dp_setED('playTime', 0, i+1);
  // }

  document.playTime = new Array(numVids).fill(0);
  document.playTimes = new Array(numVids).fill("0");
  document.prevPlayTime = new Array(numVids).fill(0);
  document.playCount = new Array(numVids).fill(0);
  document.isPlaying = new Array(numVids).fill(false); // note this indicator will be based on the intention to play, not actual playing state (eg buffering)
  // these are for tracking actual playing time as determined by the "playing" and "waiting" events (eg buffering)
  document.playingTime = new Array(numVids).fill(0);
  document.playPauseWaitTimes = new Array(numVids).fill("0");
  document.prevPlayingTime = new Array(numVids).fill(0);
  
  document.touches = [];
  document.navClicks = "";

  // now set up the function that monitors changes in the players
  // we use the transitionEnd event here because it was recommended here:
  // https://github.com/nolimits4web/swiper/issues/2864
  swiper.on('slideChange', swiperSlideChangeHandler);
  swiper.on('touchMove', swiperTouchMoveHandler);
  // don't need this: by "opposite" they actually mean "orthogonal" ie vertical instead of horizontal
  // swiper.on('touchMoveOpposite', (s, e) => {console.log('tmo'); console.log(e);});
  swiper.navigation.nextEl.addEventListener('click', swiperNavButtonHandler, true);
  swiper.navigation.prevEl.addEventListener('click', swiperNavButtonHandler, true);

  // now set formatting and event listeners for each video.
  // first decide on the size

  var video;
  //console.log('document.players:');
  //console.log(document.players);
  for (let i = 0; i < numVids; i++) {
    //player = document.players[i];
    video = document.videos[i];
    // set the video to be full-screen-ish on mobile
    // probably need to make this conditional for mobile
    video.width = videoWidth;
    video.height = videoHeight;
    video.addEventListener("play", playEventHandler);
    video.addEventListener("pause", pauseEventHandler);
    video.addEventListener("ended", pauseEventHandler);
    video.addEventListener("playing", playingEventHandler);
    video.addEventListener("waiting", waitingEventHandler);
  }

});
  
Qualtrics.SurveyEngine.addOnPageSubmit(function () {

});
  