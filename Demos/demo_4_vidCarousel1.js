Qualtrics.SurveyEngine.addOnload(function () {
  // ------------------------- addOnload() beginning of the universal cheating check code for Qualtrics.SurveyEngine.addOnload() -------------------------
  // You can put this in however many questions you want.
  // Then make sure you create embedded data fields for all of them.
  // So if you want to log this on 3 questions,
  // make the following embedded data fields:
  // CurrQLogNum = 0
  // CheatEventLog1, CheatEventLog2, CheatEventLog3
  // CheatTime1, CheatTime2, CheatTime3
  
  // make functions for easy multiple logging
  function getEmbeddedData(prefix, number) {
    if (number === undefined) {
      var name = prefix;
    } else {
      var name = prefix + parseInt(number).toString();
    }
    var temp = Qualtrics.SurveyEngine.getEmbeddedData(name);
	  //console.log('getEmbeddedData() for ' + name + ' : got ' + temp);
	  return temp;
  }
  document.dp_getED = getEmbeddedData;

  function setEmbeddedData(prefix, value, number) {
    if (number === undefined) {
      var name = prefix;
    } else {
      var name = prefix + parseInt(number).toString();
    }
    Qualtrics.SurveyEngine.setEmbeddedData(name, value);
    //console.log('setEmbeddedData(): setting ' + name + ' to ' + value);
    return name;
  }
  document.dp_setED = setEmbeddedData;

  function getEmbeddedDataSeq(prefix) {
    if (!document.dp_currQNum) {
      document.dp_currQNum = Number(Qualtrics.SurveyEngine.getEmbeddedData("CurrQLogNum"));
      if (isNaN(document.dp_currQNum)) document.dp_currQNum = 0;
    }
    const num = document.dp_currQNum;
    return getEmbeddedData(prefix, num);
  }
  document.dp_getEDseq = getEmbeddedDataSeq;

  function setEmbeddedDataSeq(prefix, value) {
    if (!document.dp_currQNum) {
      document.dp_currQNum = Number(Qualtrics.SurveyEngine.getEmbeddedData("CurrQLogNum"));
      if (isNaN(document.dp_currQNum)) document.dp_currQNum = 0;
    }
    const num = document.dp_currQNum;
    return setEmbeddedData(prefix, value, num);
  }
  document.dp_setEDseq = setEmbeddedDataSeq;

  function logCheatEvent(e) {
    console.log('logCheatEvent() got "' + e.type + '" event in currQNum ' + document.dp_currQNum + ":");
    console.log(e);
    var currLog;
    var currTime = 0;
    if (e && "target" in e && e.target) {
      var target = e.target;
      // console.log("logCheatEvent() got event with target:");
      // console.log(target);
      // handle the blur/focus event here
      if (e && e.type == "blur") {
        // they moved away from this page.
        currLog = "|blur@" + e.timeStamp.toFixed(0);
        document.cheatTimeTemp = e.timeStamp;
      } else if (e.type == "focus") {
        // they returned attention to this page.
        // check for a weird condition that sometimes happens if it starts unfocused
        if (document.cheatTimeTemp == undefined) {
          // hasn't been set yet because we were never focused before becoming blurred
          console.log('logCheatEvent(e): weird condition, focus event occurred without preceding blur. this probably means we launched blurred.');
          currLog = "|startupFocus@" + e.timeStamp.toFixed(0);
          currTime = e.timeStamp - document.beginningTime;
          console.log('logCheatEvent() focus handler calculated currTime ' + currTime + ' from e.timeStamp ' + e.timeStamp + ' and document.beginningTime ' + document.beginningTime);
        } else {
          currLog = "|focus@" + e.timeStamp.toFixed(0);
          currTime = e.timeStamp - document.cheatTimeTemp;
          console.log('logCheatEvent() focus handler calculated currTime ' + currTime + ' from e.timeStamp ' + e.timeStamp + ' and document.cheatTimeTemp ' + document.cheatTimeTemp);
        }
        document.cheatTimeTemp = e.timeStamp;
      } else {
        console.log(
          "logCheatEvent() received an unknown event type: " +
            e.type +
            " with target. This shouldn't happen!"
        );
        console.log(e.target);
      }
    } else {
      // if we are here, it means there's no event target
      if (e && e.type == "beginning") {
        console.log("logCheatEvent() Received our synthetic beginning event, current question number " + document.dp_currQNum + " in logging system.");
        currLog = "beginning@" + e.timeStamp.toFixed(0);
        document.beginningTime = e.timeStamp;
      } else if (e && e.type == "ending") {
        console.log("logCheatEvent() Received our synthetic ending event, current question number " + document.dp_currQNum + " in logging system.");
        currLog = "|ending@" + e.timeStamp.toFixed(0);
      } else {
        console.log(
          "logCheatEvent() received an unknown event type without target: " +
            e.type +
            " This shouldn't happen!"
        );
      }
    }
    // now save it
    var cheatEventLog = document.dp_getEDseq('CheatEventLog');
    var cheatTotalTime = document.dp_getEDseq('CheatTime');
    if (cheatEventLog) {
      cheatEventLog += currLog;
    } else {
      cheatEventLog = currLog;
    }
    if (cheatTotalTime) {
      cheatTotalTime = Number(cheatTotalTime) + currTime;
    } else {
      cheatTotalTime = currTime;
    }
    document.dp_setEDseq('CheatEventLog', cheatEventLog);
    document.dp_setEDseq('CheatTime', cheatTotalTime.toFixed(0));
  }
  document.logCheatEvent = logCheatEvent;

  function setupCheatLogging() {
    // This has to happen after the DOM is fully loaded, or else the events don't fire.
    // So be sure to do it in the addOnReady() section.
    //console.log('About to set event listeners for blur and focus');
    window.addEventListener("blur", document.logCheatEvent);
    window.addEventListener("focus", document.logCheatEvent);
    //console.log('Finished setting event listeners for blur and focus');
    // get the log number we are on
    var prevLogNum = Number(Qualtrics.SurveyEngine.getEmbeddedData("CurrQLogNum"));
    console.log('setupCheatLogging(): retrieved prevLogNum from embedded data: ' + prevLogNum);
    if (isNaN(prevLogNum)) {
      prevLogNum = 0;
    }
    document.dp_currQNum = prevLogNum + 1; 
    Qualtrics.SurveyEngine.setEmbeddedData("CurrQLogNum", document.dp_currQNum);
    console.log("------------------->>>>> Cheat event logging system starting: prevLogNum: " + prevLogNum + " currQNum: " + document.dp_currQNum);
    document.logCheatEvent(new Event("beginning"));
  }
  document.setupCheatLogging = setupCheatLogging;

  function shutdownCheatLogging() {
    // This seems to work fine in addOnUnload()
    // I don't know if it will also work in addOnPageSubmit() but it might!    console.log("Cheat event logging system closing out log question #" + document.dp_currQNum + ", about to increment embedded data CurrQLogNum to " + document.dp_currQNum);
    Qualtrics.SurveyEngine.setEmbeddedData("CurrQLogNum", document.dp_currQNum);
    //console.log('About to remove event listeners for blur and focus');
    window.removeEventListener("blur", document.logCheatEvent);
    window.removeEventListener("focus", document.logCheatEvent);
    console.log("------------------->>>>> Cheat event logging system ending: already set embedded data CurrQLogNum to value of document.dp_currQNum: " + document.dp_currQNum);
    document.logCheatEvent(new Event("ending"));
  }
  document.shutdownCheatLogging = shutdownCheatLogging;
  // ------------------------- end of the universal cheating check code for Qualtrics.SurveyEngine.addOnload() end -------------------------

});

Qualtrics.SurveyEngine.addOnReady(function () {
  // ------------------------- addOnReady() beginning of the universal cheating check code for Qualtrics.SurveyEngine.addOnReady() -------------------------
  // This has to happen after the DOM is fully loaded, or else the events don't fire.
  // So be sure to do it in the addOnReady() section.
  document.setupCheatLogging();
  // ------------------------- end of the universal cheating check code for Qualtrics.SurveyEngine.addOnReady() end -------------------------

});
  
Qualtrics.SurveyEngine.addOnPageSubmit(function () {

});
  