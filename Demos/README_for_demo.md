# Qualtrics Tricks!

## Introduction
Built-in Qualtrics functionality provides some advanced tools that give you a lot of flexibility and possibility for building your surveys. Some of the most powerful lesser-known features are:
- Load parameters (embedded data/piped text) from data tables uploaded to a Qualtrics library
- Load parameters via a web service
- Add custom CSS to the survey format section of the survey
- Hide the next button with a timer
- etc.

Qualtrics also allows you to add custom HTML and custom Javascript to individual questions, which opens up tremendous possibilities for customization, if you're willing to go a bit deeper into the code. This document provides code and instructions to get you started with some of these possibilities.

##DIY Qualtrics Mods:##
- [Cheating Check](#cheating-check)
- [Popups](#qualtrics-popups)
- [Vertical Slider](#vertical-slider)
- [Image or Video Carousel](#video-carousel)
##Demo links (inspirational examples)##


<a id="cheating-check"></a>
### Cheating Check
Did you ever wonder if people were taking your survey all at once, or if they were leaving it and coming back later? Do you have questions that you don't want people to cheat on, by looking things up online? 

<a id="qualtrics-popups"></a>
### Text popups
First item content goes here

<a id="vertical-slider"></a>
### Vertical Slider
Second item content goes here

<a id="video-carousel"></a>
### Image or Video Carousel
Second item content goes here
