Qualtrics.SurveyEngine.addOnload(function () {
  // Universal cheating check code
  // You can put this in however many questions you want.
  // Then make sure you create embedded data fields for all of them.
  // So if you want to log this on 3 questions,
  // make the following embedded data fields:
  // CurrQLogNum = 0
  // CheatEventLog1, CheatEventLog2, CheatEventLog3
  // CheatTime1, CheatTime2, CheatTime3
  
  // make functions for easy multiple logging
  function getEmbeddedData(prefix, number) {
    if (number === undefined) {
      var name = prefix;
    } else {
      var name = prefix + parseInt(number).toString();
    }
    var temp = Qualtrics.SurveyEngine.getEmbeddedData(name);
	  console.log('getEmbeddedData() for ' + name + ' : got ' + temp);
	  return temp;
  }
  document.dp_getED = getEmbeddedData;

  function setEmbeddedData(prefix, value, number) {
    if (number === undefined) {
      var name = prefix;
    } else {
      var name = prefix + parseInt(number).toString();
    }
    Qualtrics.SurveyEngine.setEmbeddedData(name, value);
    console.log('setEmbeddedData(): setting ' + name + ' to ' + value);
    return name;
  }
  document.dp_setED = setEmbeddedData;

  function getEmbeddedDataSeq(prefix) {
    if (!document.dp_currQNum) {
      document.dp_currQNum = Number(Qualtrics.SurveyEngine.getEmbeddedData("CurrQLogNum"));
      if (isNaN(document.dp_currQNum)) document.dp_currQNum = 0;
    }
    const num = document.dp_currQNum;
    return getEmbeddedData(prefix, num);
  }
  document.dp_getEDseq = getEmbeddedDataSeq;

  function setEmbeddedDataSeq(prefix, value) {
    if (!document.dp_currQNum) {
      document.dp_currQNum = Number(Qualtrics.SurveyEngine.getEmbeddedData("CurrQLogNum"));
      if (isNaN(document.dp_currQNum)) document.dp_currQNum = 0;
    }
    const num = document.dp_currQNum;
    return setEmbeddedData(prefix, value, num);
  }
  document.dp_setEDseq = setEmbeddedDataSeq;

  function logCheatEvent(e) {
    console.log('logCheatEvent() got "' + e.type + '" event in currQNum ' + document.dp_currQNum + ":");
    console.log(e);
    var currLog;
    var currTime = 0;
    if (e && "target" in e && e.target) {
      var target = e.target;
      // console.log("logCheatEvent() got event with target:");
      // console.log(target);
      // handle the blur/focus event here
      if (e && e.type == "blur") {
        // they moved away from this page.
        currLog = "|blur@" + e.timeStamp.toFixed(0);
        document.cheatTimeTemp = e.timeStamp;
      } else if (e.type == "focus") {
        // they returned attention to this page.
        currLog = "|focus@" + e.timeStamp.toFixed(0);
        currTime = e.timeStamp - document.cheatTimeTemp;
        console.log('logCheatEvent() focus handler calculated currTime ' + currTime + ' from e.timeStamp ' + e.timeStamp + ' and document.cheatTimeTemp ' + document.cheatTimeTemp);
        document.cheatTimeTemp = e.timeStamp;
      } else {
        console.log(
          "logCheatEvent() received an unknown event type: " +
            e.type +
            " with target. This shouldn't happen!"
        );
        console.log(e.target);
      }
    } else {
      // if we are here, it means there's no event target
      if (e && e.type == "beginning") {
        console.log("logCheatEvent() Received our synthetic beginning event, current question number " + document.dp_currQNum + " in logging system.");
        currLog = "beginning@" + e.timeStamp.toFixed(0);
      } else if (e && e.type == "ending") {
        console.log("logCheatEvent() Received our synthetic ending event, current question number " + document.dp_currQNum + " in logging system.");
        currLog = "|ending@" + e.timeStamp.toFixed(0);
      } else {
        console.log(
          "logCheatEvent() received an unknown event type without target: " +
            e.type +
            " This shouldn't happen!"
        );
      }
    }
    // now save it
    var cheatEventLog = document.dp_getEDseq('CheatEventLog');
    var cheatTotalTime = document.dp_getEDseq('CheatTime');
    if (cheatEventLog) {
      cheatEventLog += currLog;
    } else {
      cheatEventLog = currLog;
    }
    if (cheatTotalTime) {
      cheatTotalTime += currTime;
    } else {
      cheatTotalTime = currTime;
    }
    document.dp_setEDseq('CheatEventLog', cheatEventLog);
    document.dp_setEDseq('CheatTime', cheatTotalTime.toFixed(0));
  }
  document.logCheatEvent = logCheatEvent;

  function setupCheatLogging() {
    // This has to happen after the DOM is fully loaded, or else the events don't fire.
    // So be sure to do it in the addOnReady() section.
    //console.log('About to set event listeners for blur and focus');
    window.addEventListener("blur", document.logCheatEvent);
    window.addEventListener("focus", document.logCheatEvent);
    //console.log('Finished setting event listeners for blur and focus');
    // get the log number we are on
    var prevLogNum = Number(Qualtrics.SurveyEngine.getEmbeddedData("CurrQLogNum"));
    if (isNaN(prevLogNum)) {
      prevLogNum = 0;
    }
    document.dp_currQNum = prevLogNum + 1; 
    Qualtrics.SurveyEngine.setEmbeddedData("CurrQLogNum", document.dp_currQNum);
    console.log("------------------->>>>> Cheat event logging system starting: prevLogNum: " + prevLogNum + " currQNum: " + document.dp_currQNum);
    document.logCheatEvent(new Event("beginning"));
  }
  document.setupCheatLogging = setupCheatLogging;

  function shutdownCheatLogging() {
    // This seems to work fine in addOnUnload()
    // I don't know if it will also work in addOnPageSubmit() but it might!    console.log("Cheat event logging system closing out log question #" + document.dp_currQNum + ", about to increment embedded data CurrQLogNum to " + document.dp_currQNum);
    Qualtrics.SurveyEngine.setEmbeddedData("CurrQLogNum", document.dp_currQNum);
    //console.log('About to remove event listeners for blur and focus');
    window.removeEventListener("blur", document.logCheatEvent);
    window.removeEventListener("focus", document.logCheatEvent);
    //console.log('Finished removing event listeners for blur and focus');
    document.logCheatEvent(new Event("ending"));
  }
  document.shutdownCheatLogging = shutdownCheatLogging;
  // -------------------------- end of the universal cheating check code for Qualtrics.SurveyEngine.addOnload() ----------------------------



  document.supportEventLog = [];
  document.opposeEventLog = [];
  document.supportEventTime = 0;
  document.opposeEventTime = 0;
  document.articleReadCount = { opp: 0, sup: 0 };
  document.doFeeWarn = Qualtrics.SurveyEngine.getEmbeddedData("DoFeeWarn");
  console.log("initial setup: doFeeWarn is " + document.doFeeWarn);
  document.doFeeWarn = parseInt(document.doFeeWarn) ? true : false;
  console.log("initial setup: doFeeWarn is " + document.doFeeWarn);
  document.readFee = Qualtrics.SurveyEngine.getEmbeddedData("ReadFee");

  Qualtrics.SurveyEngine.setEmbeddedData("AllEssayEventLog", "");
  Qualtrics.SurveyEngine.setEmbeddedData("AllEssayTotalTime", 0);
  Qualtrics.SurveyEngine.setEmbeddedData("OpposeEssayEventLog", "");
  Qualtrics.SurveyEngine.setEmbeddedData("OpposeEssayTotalTime", 0);
  Qualtrics.SurveyEngine.setEmbeddedData("SupportEssayEventLog", "");
  Qualtrics.SurveyEngine.setEmbeddedData("SupportEssayTotalTime", 0);

  // get these texts, but set defaults here for testing purposes
  document.readFeePrewarn =
    Qualtrics.SurveyEngine.getEmbeddedData("ReadFeePrewarn");
  if (document.readFeePrewarn && document.readFeePrewarn.replace(/ /g, "")) {
    // that means it's not empty and it's not just blank space
    // so we are good here
  } else {
    // it is empty, or blank
    document.readFeePrewarn =
      "You are about to read one article for free. If you want to read the second article after this, you will be charged a fee of " +
      document.readFee;
  }

  document.readFeeWarn = Qualtrics.SurveyEngine.getEmbeddedData("ReadFeeWarn");
  if (document.readFeeWarn && document.readFeeWarn.replace(/ /g, "")) {
    // that means it's not empty and it's not just blank space
    // so we are good here
  } else {
    // it is empty, or blank
    document.readFeeWarn =
      "Since you already read the other article for free, you will now be charged a fee of " +
      document.readFee +
      " to read this article. You can read it as many times as you want once you pay the fee.";
  }

  // this is based on the destructuring assignment trick I learned here:
  // https://stackoverflow.com/questions/34698905/how-can-i-clone-a-javascript-object-except-for-one-key
  // thanks to Ilya Palkin. This is honestly pretty amazing
  // ok well it didn't work, toooooo new for Qualtrics lolololol
  function otherOne(anObj, wrongOne) {
    var keys = Object.keys(anObj);
    var newKeys = keys.filter((x) => x != wrongOne);
    if (newKeys.length == keys.length) {
      console.log("otherOne(): given item was not found in object!");
    } else if (newKeys.length == 0) {
      console.log(
        "otherOne(): for some reason nothing was left after removing given item!"
      );
    } else if (newKeys.length > 1) {
      console.log(
        "otherOne(): output has more than one item. This is fine if that's what you want but be careful!"
      );
    }
    let otherKey = newKeys[0];
    let other = anObj[otherKey];
    //let {[wrongOne]: _, ...others} = anObj;
    //let otherKey = Object.keys(others)[0];
    //let other = others[otherKey];
    return { key: otherKey, val: other };
  }

  function logEssayEvent(e) {
    console.log("logEssayEvent() got event:");
    console.log(e);
    var currLog = Qualtrics.SurveyEngine.getEmbeddedData("AllEssayEventLog");
    var currTime = Qualtrics.SurveyEngine.getEmbeddedData("AllEssayTotalTime");
    var currOpposeLog = Qualtrics.SurveyEngine.getEmbeddedData(
      "OpposeEssayEventLog"
    );
    var currOpposeTime = Number(
      Qualtrics.SurveyEngine.getEmbeddedData("OpposeEssayTotalTime")
    );
    var currSupportLog = Qualtrics.SurveyEngine.getEmbeddedData(
      "SupportEssayEventLog"
    );
    var currSupportTime = Number(
      Qualtrics.SurveyEngine.getEmbeddedData("SupportEssayTotalTime")
    );
    if ("target" in e && e.target) {
      const target = e.target;
      console.log("logEssayEvent() got event with target:");
      console.log(target);
      if (target.classList.contains("opposeOpen")) {
        console.log(currOpposeTime);
        currLog += "|opposeOpen@" + e.type + "@" + e.timeStamp.toFixed(0);
        currOpposeLog += "|opposeOpen@" + e.type + "@" + e.timeStamp.toFixed(0);
        document.opposeEventTime = e.timeStamp;
      } else if (target.classList.contains("supportOpen")) {
        console.log(currSupportTime);
        currLog += "|supportOpen@" + e.type + "@" + e.timeStamp.toFixed(0);
        currSupportLog +=
          "|supportOpen@" + e.type + "@" + e.timeStamp.toFixed(0);
        document.supportEventTime = e.timeStamp;
      } else if (target.classList.contains("opposeClose")) {
        console.log(currOpposeTime);
        currLog += "|opposeClose@" + e.type + "@" + e.timeStamp.toFixed(0);
        currOpposeLog +=
          "|opposeClose@" + e.type + "@" + e.timeStamp.toFixed(0);
        currOpposeTime += e.timeStamp - document.opposeEventTime;
        currTime += e.timeStamp - document.opposeEventTime;
        console.log(
          "logEssayEvent(): currOpposeTime about to be saved: " + currOpposeTime.toFixed(0)
        );
        document.opposeEventTime = e.timeStamp;
      } else if (target.classList.contains("supportClose")) {
        console.log(currSupportTime);
        currLog += "|supportClose@" + e.type + "@" + e.timeStamp.toFixed(0);
        currSupportLog +=
          "|supportClose@" + e.type + "@" + e.timeStamp.toFixed(0);
        currSupportTime += e.timeStamp - document.supportEventTime;
        currTime += e.timeStamp - document.supportEventTime;
        console.log(
          "logEssayEvent(): currSupportTime about to be saved: " + currSupportTime.toFixed(0)
        );
        document.supportEventTime = e.timeStamp;
      } else {
        console.log(
          "logEssayEvent() received an unknown event type with target: " +
            e.type +
            " This shouldn't happen!"
        );
      }
    } else {
      // if we are here, it means there's no event target
      if (String(e.type).toLowerCase() == "pageready") {
        console.log(
          "logEssayEvent(): Received Page Ready event."
        );
      } else {
        console.log(
          "logEssayEvent() received an unknown event type without target: " +
            e.type +
            " This shouldn't happen!"
        );
      }
    }
    Qualtrics.SurveyEngine.setEmbeddedData("AllEssayEventLog", currLog);
    Qualtrics.SurveyEngine.setEmbeddedData(
      "AllEssayTotalTime",
      parseFloat(currTime).toFixed(0)
    );
    Qualtrics.SurveyEngine.setEmbeddedData(
      "OpposeEssayEventLog",
      currOpposeLog
    );
    Qualtrics.SurveyEngine.setEmbeddedData(
      "OpposeEssayTotalTime",
      parseFloat(currOpposeTime).toFixed(0)
    );
    Qualtrics.SurveyEngine.setEmbeddedData(
      "SupportEssayEventLog",
      currSupportLog
    );
    Qualtrics.SurveyEngine.setEmbeddedData(
      "SupportEssayTotalTime",
      parseFloat(currSupportTime).toFixed(0)
    );
  }
  // set this function to a document property so we can access it globally
  document.logEssayEvent = logEssayEvent;

  // Functions for handling the modal popups for article and warnings

  function showArticle(articleType) {
    // this actually shows the requested article. Also updates the record of how many times it was shown.
    console.log("showArticle() got articleType " + articleType);
    var divId;
    if (String(articleType).toLowerCase() == "sup") {
      divId = "#myModalSupport";
    } else if (String(articleType).toLowerCase() == "opp") {
      divId = "#myModalOppose";
    }
    const article = new bootstrap.Modal(document.querySelector(divId), {});
    article.show();
    document.articleReadCount[articleType] += 1;
    return article;
  }

  function showWarning(articleType) {
    // this shows the warning dialog
    console.log("showWarning() got articleType |" + articleType + "|");
    var divId;
    if (String(articleType).toLowerCase() == "sup") {
      divId = "#warnModalCenterSup";
    } else if (String(articleType).toLowerCase() == "opp") {
      divId = "#warnModalCenterOpp";
    } else {
      console.log(
        "showWarning(): for some reason the article type isn't working: " +
          articleType
      );
    }
    const warning = new bootstrap.Modal(document.querySelector(divId), {});
    warning.show();
    return warning;
  }

  // launch article function that smartly determines whether to show the warning dialog
  function launchArticle(articleType) {
    // this is the function that is called by the user clicking the "read article" link.
    // it intelligently determines what behavior is appropriate for this version and the current state.
    console.log("launchArticle() got article type " + articleType);
    const thisReadCount = document.articleReadCount[articleType];
    const otherInfo = otherOne(document.articleReadCount, articleType);
    const otherArtType = otherInfo.key;
    const otherReadCount = otherInfo.val;
    console.log(
      "launchArticle(): Article types:read counts: this: " +
        articleType +
        ":" +
        thisReadCount +
        ", other: " +
        otherArtType +
        ":" +
        otherReadCount
    );
    //const totalReadCount = document.supportReadCount + document.opposeReadCount;
    if (!document.doFeeWarn || document.articleReadCount[articleType] > 0) {
      // if we are not doing the warning at all, or else if the requested one has already been viewed, then no more warnings are necessary
      showArticle(articleType);
      // anything past this block will necessarily have document.doFeeWarn is true
    } else if (document.articleReadCount[articleType] == 0) {
      // the requested one has not been read yet, so launch the baseline warning modal
      // if it was necessary, the message text was already updated by the code in the button handler at the end of the dialog
      showWarning(articleType);
    } else {
      // that should have covered all the possibilities, so if we get here it's an error and I'll log it
      console.log(
        "launchArticle(): got to end of if-else-else block. this should not happen. articleType: " +
          articleType +
          " articleReadCount: " +
          articleReadCount
      );
    }
  }
  document.launchArticle = launchArticle;

  function warnDialogHandler(articleType) {
    // this is the function that runs from the warning dialog when you click the yes, read button.
    // here's how it works:
    // the button's data-dismiss property means that it dismisses itself immediately from the screen
    // then the setTimeout in the onclick handler launches the new modal after 200ms
    // which is long enough to let bootstrap.Modal library do whatever it needs to do behind the scenes regarding window scrolling
    // then also this runs at that time.
    // that means that we can change the innerText of the warning dialog here without worrying about it flashing on the screen
    // because it's already dismissed 200ms before this runs
    console.log("warnDialogHandler() got article type " + articleType);
    if (
      document.articleReadCount.opp == 0 &&
      document.articleReadCount.sup == 0
    ) {
      // nothing has been read yet, this is the first time saying yes.
      // this is one category of condition but we still need to know which one is which
      if (articleType == "opp") {
        document.getElementById("readFeeWarnSup").innerText =
          document.readFeeWarn;
      } else if (articleType == "sup") {
        document.getElementById("readFeeWarnOpp").innerText =
          document.readFeeWarn;
      } else {
        console.log(
          "warnDialogHandler() ERROR got an unknown article type, this should never happen! " +
            articleType
        );
      }
    } else {
      // we disable this elsewhere when the warnings are done,
      // so the only way we ever end up in this else block is if we are showing the full warning
      // which means it's time to actually charge the fee
      console.log(
        "warnDialogHandler() is now setting the embedded data variable PaymentSubtractValue to " +
          document.readFee
      );
      if (document.readFee) {
        Qualtrics.SurveyEngine.setEmbeddedData(
          "PaymentSubtractValue",
          parseFloat(document.readFee.match(/\d+/)[0]).toFixed(2)
        );
      }
    }
    document.articleReadCount[articleType] += 1;
    Qualtrics.SurveyEngine.setEmbeddedData(
      "OpposeEssayReadCount",
      document.articleReadCount["opp"].toFixed(0)
    );
    Qualtrics.SurveyEngine.setEmbeddedData(
      "SupportEssayReadCount",
      document.articleReadCount["sup"].toFixed(0)
    );
    showArticle(articleType);
  }
  document.warnDialogHandler = warnDialogHandler;
});

Qualtrics.SurveyEngine.addOnReady(function () {
  // This has to happen after the DOM is fully loaded, or else the events don't fire.
  // So be sure to do it in the addOnReady() section.
  document.setupCheatLogging();
  // -------------------------- end of the universal cheating check code for Qualtrics.SurveyEngine.addOnReady() ----------------------------

  // need to do this per Stack Overflow
  document.body.appendChild(document.getElementById("warnModalCenterSup"));
  document.body.appendChild(document.getElementById("warnModalCenterOpp"));
  document.body.appendChild(document.getElementById("myModalSupport"));
  document.body.appendChild(document.getElementById("myModalOppose"));
  //addEventListener("beforeunload", e => {console.log(e); e.preventDefault();}, true);

  document.logEssayEvent(new Event("pageReady"));
  const essayButtons = document.querySelectorAll(
    ".supportOpen,.supportClose,.opposeOpen,.opposeClose"
  );
  console.log("essayButtons:");
  console.log(essayButtons);
  for (b of essayButtons) {
    console.log("adding pointerDown event");
    b.addEventListener("pointerDown", document.logEssayEvent);
    console.log("adding click event");
    b.addEventListener("click", document.logEssayEvent);
    console.log(b);
  }
  // set the dialog text to the right text
  if (document.doFeeWarn) {
    document.getElementById("readFeeWarnOpp").innerText =
      document.readFeePrewarn;
    document.getElementById("readFeeWarnSup").innerText =
      document.readFeePrewarn;
  }
});

Qualtrics.SurveyEngine.addOnUnload(function () {
  // need to get rid of these, otherwise they linger after Qualtrics engine reloads the next block

  document.getElementById("warnModalCenterSup").remove();
  document.getElementById("warnModalCenterOpp").remove();
  document.getElementById("myModalSupport").remove();
  document.getElementById("myModalOppose").remove();
  
  // This seems to work fine in addOnUnload()
  // I don't know if it will also work in addOnPageSubmit() but it might!
  document.shutdownCheatLogging();
  // -------------------------- end of the universal cheating check code for Qualtrics.SurveyEngine.addOnPageSubmit() ----------------------------

});
