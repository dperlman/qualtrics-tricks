import re
import sys, os, os.path, glob

regexes = []
#regexes.append(r'^\s\s//\sUniversal(.|\n)+?-{25}\n')
#regexes.append(r'^\s\s//\saddOnReady\scode(.|\n)+?-{25}\n')
#regexes.append(r'^\s\s//\saddOnPageSubmit\scode(.|\n)+?-{25}\n')
regexes.append(r'^\s\s//\s-{25}\saddOnload\(\)\sbeginning.*addOnload\(\)(.|\n)+?addOnload\(\)\send\s-{25}\n')
regexes.append(r'^\s\s//\s-{25}\saddOnReady\(\)\sbeginning.*addOnReady\(\)(.|\n)+?addOnReady\(\)\send\s-{25}\n')
regexes.append(r'^\s\s//\s-{25}\saddOnPageSubmit\(\)\sbeginning.*addOnPageSubmit\(\)(.|\n)+?addOnPageSubmit\(\)\send\s-{25}\n')

mainDir = os.getcwd().split('qualtrics-tricks')[0]
mainDir = os.path.join(mainDir, 'qualtrics-tricks')
sourceDir = os.path.join(mainDir, 'CheatCheck')
replaceDir = os.path.join(mainDir, 'Demos')
sourceFile = os.path.join(sourceDir, 'cheatCheckUniversal.js')
replacePath = os.path.join(replaceDir, 'demo*.js')
replaceFiles = glob.glob(replacePath)
print(replaceFiles)

with open(sourceFile) as f:
  stxt = f.read()


for r in regexes:
  print(r)
  rtxt = re.search(r, stxt, re.MULTILINE).group()
  # escape any \ for use in re.sub. per example in re.escape documentation.
  rtxt = rtxt.replace('\\', r'\\')
  #print(rtxt)
  for rf in replaceFiles:
    with open(rf, 'r') as rff:
      rftxt = rff.read()
    rftxtnew = re.sub(r, rtxt, rftxt, 1, re.MULTILINE)
    print(rftxtnew)
    with open(rf, 'w') as rff:
      rff.write(rftxtnew)


  