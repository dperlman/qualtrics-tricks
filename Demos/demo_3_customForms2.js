Qualtrics.SurveyEngine.addOnload(function() {
  /*Place your JavaScript here to run when the page is fully displayed*/
});


Qualtrics.SurveyEngine.addOnReady(function() {
  function miubSetupProposerChoice() {
    makeChoiceStorage();
    miubHideNextButton();
    miubAddEllipsesToDOM();
    setTableHandlers();
  }
  
  function miubSetupFixedPie(that) {
    miubMoveFormInputs();
    //miubSetCallbackNextButton(that, miubTestFixedPieReady);
  }
  
  function miubSetupResponderSeesProposal() {
    miubDecodeChoiceString();
    miubVoiceCount();
    miubAddEllipsesToDOM();
    miubHighlightFromChoices();
  }
  
  function miubSetupResponderVoice() {
    miubDecodeChoiceString();
    //miubHideNextButton();
    miubAddEllipsesToDOM();
    miubHighlightFromChoices();
    setTableHandlers();
  }
  
  function miubSetupResponderDecision() {
    miubDecodeChoiceString();
    miubAddEllipsesToDOM();
    miubHighlightFromChoices();
  }
  
  
  function miubSetupIOSscale(that) {
    miubIOS_HTML_SVG(that);
  }

  function setTableHandlers() {
    var tables = document.querySelectorAll("table.miubOutertable");
    for (const t of tables) {
      //console.log(t.nodeName, t.className);
      t.addEventListener('click', miubBubbleHighlight);
    }
  }
  
  function miubEllipseElement() {
    var ellipseHtml = '<div class="miubSelected"><svg><ellipse cx="50%" cy="50%" rx="50%" ry="45%" fill="transparent"/></svg></div>'
    // learned this from here
    // https://stackoverflow.com/questions/494143/creating-a-new-dom-element-from-an-html-string-using-built-in-dom-methods-or-pro
    var template = document.createElement('template');
    template.innerHTML = ellipseHtml;
    var addEl = template.content.firstElementChild;
    return addEl;
  }
  
  function miubAddEllipsesToDOM() {
    var destinations = document.querySelectorAll("table.miubSubtable td.miubValue div.miubTableCell");
    //console.log(destinations);
    console.log("miubAddEllipsesToDOM() adding ellipses to " + destinations.length + " table elements.");
    destinations.forEach(x => x.appendChild(miubEllipseElement()));
    destinations = document.querySelectorAll("table.miubSubtable td.miubRespDecide div");
    destinations.forEach(x => x.appendChild(miubEllipseElement()));
  }
  
  function makeChoiceStorage() {
    document.miubChoiceObject = {
      "miubIssue1": null,
      "miubIssue2": null,
      "miubIssue3": null,
      "miubIssue4": null,
      "miubIssue5": null
    }
    document.miubChoiceString = "";
  }
  
  
  function miubMoveFormInputs() {
    var fieldlist = miubGetFormInputs();
    var destlist = miubGetTableOtherCells();
    var n = Math.min(fieldlist.length, destlist.length);
    console.log(" We will move " + n + " input fields onto the table.");
    for (let i=0; i<n; i++) {
      destlist[i].innerHTML = "";
      destlist[i].append(fieldlist[i]);
      fieldlist[i].style.width = '5em';
      fieldlist[i].type = "number";
      
    }
    var oldQ = document.querySelector("div#Questions div.Inner.FORM div.QuestionBody");
    oldQ.style.display = 'none';
  }
  
  function miubDecodeChoiceString() {
    var cstr;
    if (document.miubChoiceString) {
      cstr = document.miubChoiceString;
    } else {
      cstr = Qualtrics.SurveyEngine.getEmbeddedData("proposal");
    }
    if (!cstr) {
      makeChoiceStorage();
      console.log("no input string, made empty storage");
      return;
    }
    var choiceArray = cstr.split("|");
    console.log(choiceArray);
    var choiceObject = {};
    document.miubFixedChoiceArray = [];
    document.miubResponderAvailableChoices = [];
    var a, f;
    for (let i = 0; i < choiceArray.length; i++) {
      a = choiceArray[i];
      if (!a) a = null;
      if (i==5) {
        f = "miubRespDec";
      } else {
        f = "miubIssue" + (i + 1);
      }
      choiceObject[f] = a;
      if (a && (a != "miubRespDecide")) {
        document.miubFixedChoiceArray.push(f);
      } else {
        document.miubResponderAvailableChoices.push(f);
      }
    }
    console.log("decoded object:", choiceObject);
    document.miubChoiceObject = choiceObject;
    console.log("Fixed choices: ", document.miubFixedChoiceArray);
    console.log("Responder free choices: ", document.miubResponderAvailableChoices);
  }
  
  function miubHighlightFromChoices() {
    var v, elList, highlightRow, issueSubtable;
    for (let k of Object.keys(document.miubChoiceObject)) {
      //console.log(k);
      v = document.miubChoiceObject[k];
      //console.log(v);
      if (!v) {
        //console.log("no value set, continuing");
        continue;
      }
      issueSubtable = document.querySelector("table.miubOutertable table." + k);
      // the for loop turns off all the ellipses first
      elList = issueSubtable.querySelectorAll("tr td div.miubSelected svg ellipse");
      for (const el of elList) {
        el.style.strokeWidth = 0;
      }
      // now we turn on just the one we want
      highlightRow = issueSubtable.querySelector("tr." + v);
      //console.log(highlightRow.nodeName, [...highlightRow.classList.values()]);
      miubHighlight(highlightRow);
    }
  }
  
  
  
  function miubBubbleHighlight(e) {
    e = e || window.event; // probably don't need this
    console.log('miubBubbleHighlight()');
    var t = e.target || e.srcElement;
    // I have click disabled on all the stuff I don't want, in the CSS
    // so I know what level of the DOM we are at and can go up 2 parent levels
    var chosenRow = t.parentElement.parentElement;
    //document.tempChosenRow = chosenRow;
    //console.log(chosenRow.nodeName, chosenRow.className);
    if (!chosenRow.classList.contains("miubOptionRow")) return;
    //console.log(chosenRow.nodeName, chosenRow.className);
    // next 2 lines gets the class that starts with "miubIssue"
    var issueClasses = [...chosenRow.parentElement.parentElement.classList.values()];
    var chosenIssue = issueClasses.find(x => x.includes("miubIssue"));
    // this only matters for the responder voice choice.
    // document.miubFixedChoiceArray will only exist if this is the responder voice screen, not if it's the proposer choice screen.
    // and if they choose one that's in the "fixed" list, then we ignore it.
    if (document.miubFixedChoiceArray && document.miubFixedChoiceArray.includes(chosenIssue)) {
      return;
    }
    // next 2 lines gets the class of the choice like "miubC1"
    var optionClasses = [...chosenRow.classList.values()];
    //var chosenChoice = optionClasses.find(x => (x.includes("miub") && !x.includes("OptionRow")));
    console.log(chosenRow);
    var chosenChoice = chosenRow.querySelectorAll('td')[0].innerText;
    if (chosenChoice == '-') {
      chosenChoice = chosenRow.querySelector('td').innerText;
    }
    //console.log(chosenIssue, chosenChoice); // this is the actual answer we want here
    // also, if they try to re-choose "miubRespDecide" again, then we ignore that too.
    // if (document.miubFixedChoiceArray && (chosenChoice == "miubRespDecide")) {
    //   return;
    // }

    document.miubChoiceObject[chosenIssue] = chosenChoice;
    miubHighlight(chosenRow);
    miubReport(); // this gets its info from document.miubChoiceObject
    miubVoiceCount();
    miubCalcChoiceString();
    if (miubTestProfitTableReady()) {
      miubShowNextButton();
    }
    console.log(document.miubChoiceObject);
    console.log(document.miubVoiceCount);
    console.log(document.miubChoiceString);
  }
  
  function miubReport() {
    const reportQ = document.evaluate('//div[contains(@class, "QuestionText") and contains(., "The selected choices are:")]', document, null, XPathResult.ANY_TYPE, null ).iterateNext();
    const choicesTable = reportQ.querySelectorAll('table tbody tr td:last-of-type');
    //const choices = Object.values(document.miubChoiceObject).map(x => x?x.slice(-1):"");
    const choices = Object.values(document.miubChoiceObject).map(x => x?x:"");
    choicesTable.forEach((x,i) => x.innerText = choices[i])

  }
  
  function miubTestProfitTableReady() {
    // need to make sure document.miubChoiceObject has no nulls
    // also need to make sure that the second round leaves no miubRespDecide choices
    // if document.miubFixedChoiceArray is undefined, then we don't worry about that
    // but if it exists as an array then we check to make sure the non-fixed ones are picked
    var v = Object.values(document.miubChoiceObject);
    if (v.includes(null)) {
      return false;
    } else if (document.miubResponderAvailableChoices && v.includes("miubRespDecide")) {
      return false;
    } else {
      return true;
    }
  }
  

  function miubTestFixedPieReady() {
    var fieldMin = Number(Qualtrics.SurveyEngine.getEmbeddedData("fixedPieFieldMin"));
    var fieldMax = Number(Qualtrics.SurveyEngine.getEmbeddedData("fixedPieFieldMax"));
    console.log("fieldMin: " + fieldMin + " fieldMax: " + fieldMax);
    var fieldlist = miubGetFormInputs();
    console.log("this is the fieldlist right now, for the validation:");
    console.log(fieldlist);
    //var validationElement = document.querySelector('div.ValidationError');
    var valid = true;
    var v;
    var i = 1;
    for (let f of fieldlist) {
      v = f.value;
      //if (isNaN(v) || v == '') {
      if (isNaN(v)) {
        valid=false;
        console.log("fixed pie input field " + i + " is: '" + v + "' which is not a number.");
        miubFixedPieValidationElement('Please ONLY enter numbers!', 'inline');
        break;
      }
      if (v == '') {
        valid=false;
        console.log("fixed pie input field " + i + " is: '" + v + "' which is blank!");
        miubFixedPieValidationElement('Please enter numbers in every field.', 'inline');
        break;
      }
      v = Number(v);
      if (v < fieldMin) {
        valid=false;
        console.log("fixed pie input field " + i + " is: " + v + " which is TOO SMALL.");
        miubFixedPieValidationElement('Please only use numbers greater than or equal to ' + fieldMin, 'inline');
        break;
      }
      if (v > fieldMax) {
        valid=false;
        console.log("fixed pie input field " + i + " is: " + v + " which is TOO LARGE.");
        miubFixedPieValidationElement('Please only use numbers less than or equal to ' + fieldMax, 'inline');
        break;
      }
      console.log("fixed pie input field " + i + " is: " + v + " which is valid.");
      i++;
    }
    // now we have the answer
    return valid;
  }
  

  function miubFixedPieValidationElement(txt, disp) {
    // returns the validation element for the Fixed Pie table
    var el = document.querySelector('table.miubOutertable').closest('div.QuestionOuter').querySelector('div.ValidationError');
    el.innerText = txt;
    el.style.display = disp;
    return el;
  }


  function miubVoiceCount() {
    var v = 0;
    for (let c of Object.values(document.miubChoiceObject)) {
      if (c == "miubRespDecide") {
        v++;
      }
    }
    document.miubVoiceCount = v;
    Qualtrics.SurveyEngine.setEmbeddedData("P_gave_voice_count", document.miubVoiceCount);
  }
  
  function miubCalcChoiceString() {
    document.miubChoiceString = Object.values(document.miubChoiceObject).join('|');
    Qualtrics.SurveyEngine.setEmbeddedData("proposal", document.miubChoiceString);
  }
  
  function miubHighlight(row) {
    var elList = row.parentElement.querySelectorAll("tr td div.miubSelected svg ellipse");
    for (const el of elList) {
      el.style.strokeWidth = 0;
    }
    var myEllipse = row.querySelector("div.miubSelected svg ellipse");
    myEllipse.style.strokeWidth = 8;
  }
  
  function miubGetFormInputs() {
    // this works before moving them, to get the IDs of the form fields
    var miubQidLocus = document.querySelector("div#Questions div.Inner.FORM div.QuestionBody input");
    if (miubQidLocus) {
      var miubQid = miubQidLocus.id.split('~').slice(1,-1).join('~');
      console.log('getting form list from original QID ' + miubQid);
      var nodelist = document.querySelectorAll("div#" + miubQid + " input");
    } else {
      // this works after moving them
      console.log('getting form list from div.miubTableCell input.InputText');
      var nodelist = document.querySelectorAll("div.miubTableCell input.InputText");
    }        
    //console.log(nodelist);
    return nodelist;
  }
  
  function miubGetTableOtherCells() {
    var cells=[];
    var itxt, stxt, ctmp;
    for (let i=1; i <= 5; i++) {
      itxt = "miubIssue" + i;
      stxt = "table.miubSubtable." + itxt + " tr.miubOptionRow td:last-of-type div.miubTableCell";
      ctmp = document.querySelectorAll(stxt);
      cells.push(...ctmp);
    }
    //console.log(cells);
    return cells;
  }
  
  function miubHideNextButton() {
    console.log("Hiding next button!");
    document.querySelector("#NextButton.NextButton.Button").setStyle("visibility:hidden");
  }
  
  function miubShowNextButton() {
    console.log("Revealing next button!");
    document.querySelector("#NextButton.NextButton.Button").setStyle("visibility:visible");
  }

  function miubSetCallbackNextButton(that, f) {
    console.log("Setting callback on next button:");
    //console.log(f);
    var nb = document.querySelector("#NextButton.NextButton.Button");
    var cloneNb = nb.cloneNode();
    cloneNb.id = 'NextButtonClone';
    nb.after(cloneNb);
    that.hideNextButton();
    cloneNb.style.display = '';
    var newListener = function (t) {
      if (f(t)) {
        console.log('Passed test: proceeding to actual next button');
        that.clickNextButton();
      } else {
        console.log('Next button click did NOT pass test');

      }
    }
    //console.log(newListener);
    cloneNb.addEventListener('click', newListener);
    cloneNb.addEventListener('keypress', newListener);
  }


  function miubIOS_HTML_SVG(that) {
    var circ1 = [39.2, 48.5, 55.0, 62.3, 66.2, 71.3, 75.7];
    var circ2 = [120.9, 111.5, 105.1, 97.7, 93.8, 88.7, 84.3];
    var text1 = [24.9, 34.0, 27.5, 29.6, 34.0, 38.9, 43.8];
    var text2 = [120.15, 105.8, 100.0, 106.3, 106.5, 101.9, 97.7];
    
    var miubClassNumber, circ1x, circ2x, text1x, text2x;
    var elem;
    

    // hopefully this always gets the question choice items. it probably won't work if there are multiple questions on the page but we can fix that
    var eList = Array(...document.querySelectorAll("#" + that.questionId + " td.LabelContainer span.LabelWrapper label.SingleAnswer"));
    console.log(eList);
    
    for (let i = 0; i < eList.length; i++) {
      console.log(i);
      miubClassNumber = "miubIOS" + (i+1);
      miubIOSnumber = (i+1);
      circ1x = circ1[i];
      circ2x = circ2[i];
      text1x = text1[i];
      text2x = text2[i];
      svgtext = `
      <td class="miubIOScell ` + miubClassNumber + `">
      <svg viewBox="0 0 160 100">
      <rect height="100" width="160" x="0" y="0" stroke="none" fill="transparent" />
      <circle cx="` + circ1x + `" cy="39.51" r="38.33" stroke="black" stroke-width="2" fill="transparent" />
      <circle cx="` + circ2x + `" cy="39.51" r="38.33" stroke="black" stroke-width="2" fill="transparent" />
      <text fill="#000000" font-family="ArialMT" font-size="14" opacity="1" text-anchor="start" x="` + text1x + `" y="41.7">You</text>
      <text fill="#000000" font-family="ArialMT" font-size="14" opacity="1" text-anchor="start" x="` + text2x + `" y="41.7">X</text>
      <text fill="#000000" font-family="ArialMT" font-size="14" opacity="1" text-anchor="start" x="75.2" y="99.4">` + miubIOSnumber + `</text>
      </svg>
      </td>
      `;
      
      elem = eList[i];
      console.log(elem);
      elem.innerHTML = svgtext;
    }
      
      
  }
  
  // Run the appropriate setup function here for the block in question
  // New in version 06: identification is done using a span in the text portion of the question.
  // The span has this format:
  // <span class='miubQuestionIdentifier' id='miubProposerChoice'></span>
  // Then the identifier can be found with 
  // document.querySelector('span.miubQuestionIdentifier').id

  var miubQid = document.querySelector('span.miubQuestionIdentifier').id;
  console.log('miubQuestionIdentifier is: ' + miubQid);

  if (miubQid == 'miubProposerChoice') {
    console.log("item text tells us to run miubSetupProposerChoice()");
    miubSetupProposerChoice();
  } else if (miubQid == 'miubFixedPie') {
    console.log("item text tells us to run miubSetupFixedPie(this)");
    miubSetupFixedPie(this);
  } else if (miubQid == 'miubResponderSeesProposal') {
    console.log("item text tells us to run miubSetupResponderSeesProposal()");
    miubSetupResponderSeesProposal();
  } else if (miubQid == 'miubResponderVoice') {
    console.log("item text tells us to run miubSetupResponderVoice()");
    miubSetupResponderVoice();
  } else if (miubQid == 'miubResponderDecision') {
    console.log("item text tells us to run miubSetupResponderDecision()");
    miubSetupResponderDecision();
  } else if ((miubQid == 'miubIosPre') || (miubQid == 'miubIosPost')) {
    console.log("item text tells us to run miubSetupIOSscale(this)");
    miubSetupIOSscale(this);
  }
});

Qualtrics.SurveyEngine.addOnPageSubmit(function() {
  
  function miubCalcSubmitScores(that) {
    console.log("Inside miubCalcSubmitScores()");
    console.log(that);
    // need to make sure you first do miubDecodeChoiceString()
    // or some other thing to make sure the document.miubChoiceObject exists
    var currIss, currChoice, pEmbed, rEmbed, pPts, rPts;
    var retObj = {
      'pScore': 0,
      'rScore': 0
    };
    console.log(that.getSelectedChoices());
    console.log(document.querySelectorAll("#" + that.questionId + " .ChoiceStructure span.LabelWrapper label.SingleAnswer span"));
    var chosenElement = document.querySelector("#" + that.questionId + " .ChoiceStructure span.LabelWrapper label.q-checked");
    console.log(chosenElement);
    if (chosenElement) {
      var chosenText = chosenElement.parentElement.querySelector("label.SingleAnswer span").innerText;
      var chosenNumber = Number(that.getSelectedChoices()[0]);
    } else {
      var chosenText = "";
      var chosenNumber = 0;
    }
    console.log(chosenText);
    console.log(chosenNumber);
    if (chosenText.toLowerCase().includes("accept") || chosenNumber == 4) {
      var decision = 1;
    } else if (chosenText.toLowerCase().includes("reject") || chosenNumber == 5) {
      var decision = 2;
    } else {
      var decision = 0;
    }
    console.log(decision);
    if (decision == 2) {
      console.log("Decision was reject, so both scores are zero");
      pPts = 0;
      rPts = 0;
    } else if (decision == 1) {
      for (let k=0; k<5; k++) {
        currIss = Object.keys(document.miubChoiceObject)[k];
        currChoice = document.miubChoiceObject[currIss];
        currChoice = currChoice[currChoice.length-1]; // just the digit
        pEmbed = "P_I" + (k+1) + "C" + currChoice + "Pts";
        rEmbed = "R_I" + (k+1) + "C" + currChoice + "Pts";
        console.log(pEmbed, rEmbed);
        pPts = Number(Qualtrics.SurveyEngine.getEmbeddedData(pEmbed));
        rPts = Number(Qualtrics.SurveyEngine.getEmbeddedData(rEmbed));
        console.log(pPts, rPts);
        retObj.pScore += pPts;
        retObj.rScore += rPts;
      }
      console.log(retObj);
    } else {
      console.log("Decision was an invalid value: " + decision);
    }
    Qualtrics.SurveyEngine.setEmbeddedData('pScore', retObj.pScore);
    Qualtrics.SurveyEngine.setEmbeddedData('rScore', retObj.rScore);
    // calculate an encoded response to send back with Smartriqs
    var response = 10 * retObj.pScore + decision;
    Qualtrics.SurveyEngine.setEmbeddedData('response', response);
    Qualtrics.SurveyEngine.setEmbeddedData('decision', decision); // this is used locally in branch logic so we still need it
    // make sure not to overwrite this in the embedded data block
    return retObj;
  }
  
  // Run the appropriate setup function here for the block in question
  var qelement = document.querySelector("#" + this.questionId + " div.QuestionText");
  if (!qelement) {
    qelement = document.querySelector("#" + this.questionId + " label.QuestionText");
  }
  var qtxt = qelement.textContent;
  if (qtxt.includes("final proposal is shown above, including your choices")) {
    console.log("item text tells us to run miubCalcSubmitScores()");
    miubCalcSubmitScores(this);
  }
});