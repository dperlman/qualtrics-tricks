Qualtrics.SurveyEngine.addOnload(function() {
  var vSlider = document.getElementById('uiSlider');
  noUiSlider.create(vSlider, {
    start: 50,
    step: 1,
    direction: 'rtl',
    orientation: 'vertical',  
    range: {
      min: 0,
      max: 100
    },
    format: wNumb({
      decimals: 0
    }),
    // Show a scale with the slider
    pips: {
        mode: 'positions',
        values: [0,10,20,30,40,50,60,70, 80,90,100],
        stepped: true,
    }
  });
    var query = "input[id^='QR~"+this.questionId+"']";
    //console.log(query);
  var inputNumber = document.querySelector(query);
    //console.log(inputNumber);
  vSlider.noUiSlider.on('update', function (values, handle) {
      inputNumber.value = values[handle];
  });
  inputNumber.addEventListener('change', function () {
      vSlider.noUiSlider.set([null, this.value]);
  });
  inputNumber.setAttribute('readonly',true)
});
 
Qualtrics.SurveyEngine.addOnReady(function()
{
  /*Place your JavaScript here to run when the page is fully displayed*/

});

Qualtrics.SurveyEngine.addOnPageSubmit(function () {
  // ------------------------- addOnPageSubmit() beginning of the universal cheating check code for Qualtrics.SurveyEngine.addOnPageSubmit() -------------------------
  // This seems to work fine in addOnUnload()
  // I think it also works fine in addOnPageSubmit()
  document.shutdownCheatLogging();
  // ------------------------- end of the universal cheating check code for Qualtrics.SurveyEngine.addOnPageSubmit() end -------------------------

});

